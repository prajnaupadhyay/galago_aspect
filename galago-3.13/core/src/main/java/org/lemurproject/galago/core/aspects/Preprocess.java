package org.lemurproject.galago.core.aspects;

import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.retrieval.processing.*;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.ScoreIterator;
import org.lemurproject.galago.core.retrieval.query.Node;

import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.io.*;



/**
 * includes modules to compute probability distributions for the parameters in the aspect-based retrieval model
 * This code reads the database located at a remote machine. To do so, first run the following command at the system where this code will be run.
 * "ssh -p 32790 -L 3307:127.0.0.1:3306 root@10.17.50.132"
 * 		- root@10.17.50.132 is the username and IP address of the remote machine. 32790 is the port where it is hosted.
 * 	    - Now this code will read the database in the remote machine as if it is located in the local machine.
 *
 */

public class Preprocess
{
    public Preprocess()
    {

    }
    public static HashMap<String, Double> estimateProbabilitiesUsingGroundTruth(String file1, String file2, String stop_words, String aspect) throws Exception
    {
        ArrayList<String> lines_aspect = readFilesEvaluated(file1+"/"+aspect+".txt");
        ArrayList<String> lines_aspect1 = readFilesSearched(file2+"/"+aspect+".txt");

        HashMap<String, Double> prob_dist = new HashMap<String, Double>();

        ArrayList<String> all_lines = new ArrayList<String>();
        all_lines.addAll(lines_aspect);
        all_lines.addAll(lines_aspect1);

        BufferedReader br1 = new BufferedReader(new FileReader(stop_words));
        HashSet<String> stop_words_list = new HashSet<String>();
        String line;
        while((line=br1.readLine())!=null)
        {
            stop_words_list.add(line);
        }

        return computeDistribution(all_lines, stop_words_list);
    }

    public static ArrayList<String> readFilesEvaluated(String file) throws Exception
    {
        BufferedReader br1 = new BufferedReader(new FileReader(file));

        String line;
        ArrayList<String> lines = new ArrayList<String>();
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().toLowerCase().replace("<strong>","").replace("</strong>","").replaceAll("[(),.!?;:]", " $0 ").replaceAll("[^A-Za-z0-9. ]+", " $0 "));
            }
            if(tokens.size()!=2) continue;
            lines.add(tokens.get(0)+" "+contentForXMLTag("paperabstract", tokens.get(1)));

        }
        return lines;
    }

    public static ArrayList<String> readFilesSearched(String file) throws Exception
    {
        BufferedReader br1 = new BufferedReader(new FileReader(file));

        String line;
        ArrayList<String> lines = new ArrayList<String>();
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().toLowerCase().replace("<strong>","").replace("</strong>","").replaceAll("[(),.!?;:]", " $0 ").replaceAll("[^A-Za-z0-9. ]+", " $0 "));
            }
            if(tokens.size()!=2) continue;
            lines.add(tokens.get(0)+" "+tokens.get(1));

        }
        return lines;
    }

    public static String contentForXMLTag(String tag, String text)
    {
        String start_tag = "<"+tag+">";
        String end_tag = "</"+tag+">";
        int start = text.indexOf(start_tag);
        int end = text.indexOf(end_tag);
        if(start>=0 && end >=0)
        {
            return text.substring(start+start_tag.length(),end);
        }
        else
        {
            return "";
        }
    }

    public static HashMap<String, Double> computeDistribution(ArrayList<String> list, HashSet<String> stop_words)
    {

        HashMap<String, Double> scores = new HashMap<String, Double>();
        double sum=0;
        for(String s: list)
        {
            StringTokenizer tok = new StringTokenizer(s," ");
            while(tok.hasMoreTokens())
            {
                String a = tok.nextToken();
                if(stop_words.contains(a)) continue;
                sum = sum+1;
                if (scores.get(a)==null)
                {
                    scores.put(a,1.0);
                }
                else
                {
                    scores.put(a,scores.get(a)+1.0);
                }
            }

        }
        HashMap<String, Double> prob_dist = new HashMap<String, Double>();
        for(String s:scores.keySet())
        {
            prob_dist.put(s,scores.get(s)/sum);
        }

        return prob_dist;

    }

    public static void computeMixtureDistribution(String word_prob_file, String entity_prob_file, String entity_prob_file1,  double lambda1, double lambda2, boolean infer, String query, String mixturefile,  String termstatistics, HashMap<Integer, Double> term_statistics, HashMap<String, Integer> term_statistics_mapped) throws Exception
    {
        BufferedReader br1 = new BufferedReader(new FileReader(word_prob_file));
        BufferedReader br2 = new BufferedReader(new FileReader(entity_prob_file));
        BufferedReader br4 = new BufferedReader(new FileReader(entity_prob_file1));



        HashMap<String, HashMap<String, Double>> aspect_scores = new HashMap<String, HashMap<String, Double>>();
        HashMap<String, HashMap<String, Double>> aspect_scores1 = new HashMap<String, HashMap<String, Double>>();
        HashMap<Integer, Double> word_scores = new HashMap<Integer, Double>();


        double aspect_score_sum=0;
        String line;

        double word_sum=0;

        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," "));
                //entities.add(to)
            }

            if(term_statistics_mapped.get(tokens.get(0))!=null)
            {
                word_scores.put(term_statistics_mapped.get(tokens.get(0)), Double.parseDouble(tokens.get(1)));
                word_sum = word_sum + Double.parseDouble(tokens.get(1));
            }

        }

        HashMap<Integer, Double> word_prob = new HashMap<Integer, Double>();
        for(int s:word_scores.keySet())
        {
            word_prob.put(s,word_scores.get(s)/word_sum);
        }

        while((line=br2.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," "));
            }
            if(tokens.size()!=3) continue;
            HashMap<String, Double> scores;
            if(aspect_scores.get(tokens.get(0))!=null)
            {
                scores = aspect_scores.get(tokens.get(0));
            }
            else
            {
                scores = new HashMap<String, Double>();
            }
            //System.out.println(scores);
            scores.put(tokens.get(1),Double.parseDouble(tokens.get(2)));
            aspect_scores.put(tokens.get(0),scores);
            aspect_score_sum = aspect_score_sum + Double.parseDouble(tokens.get(2));
        }
        HashMap<String, HashMap<String, Double>> aspect_prob = new HashMap<String, HashMap<String, Double>>();

        for(String i:aspect_scores.keySet())
        {
            HashMap<String, Double> new_scores = new HashMap<String, Double>();
            for(String j:aspect_scores.get(i).keySet())
            {
                double sc=aspect_scores.get(i).get(j)/aspect_score_sum;
                new_scores.put(j,sc);
            }
            aspect_prob.put(i, new_scores);
        }

        aspect_score_sum=0;
        while((line=br4.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," "));
                //entities.add(to)
            }
            if(tokens.size()!=3) continue;
            HashMap<String, Double> scores;
            if(aspect_scores1.get(tokens.get(0))!=null)
            {
                scores = aspect_scores1.get(tokens.get(0));
            }
            else
            {
                scores = new HashMap<String, Double>();
            }
            //System.out.println(scores);
            scores.put(tokens.get(1),Double.parseDouble(tokens.get(2)));
            aspect_scores1.put(tokens.get(0),scores);
            aspect_score_sum = aspect_score_sum + Double.parseDouble(tokens.get(2));
        }
        HashMap<String, HashMap<String, Double>> aspect_prob1 = new HashMap<String, HashMap<String, Double>>();

        for(String i:aspect_scores1.keySet())
        {
            HashMap<String, Double> new_scores = new HashMap<String, Double>();
            for(String j:aspect_scores1.get(i).keySet())
            {
                double sc=aspect_scores1.get(i).get(j)/aspect_score_sum;
                new_scores.put(j,sc);
            }
            aspect_prob1.put(i, new_scores);
        }



        System.out.println("started mixing");
        HashMap<Integer, Double> mixture = new HashMap<Integer, Double>();

        for(int s:word_prob.keySet())
        {
            mixture.put(s,lambda1*word_prob.get(s));
        }

        double alpha=1.0;

        if(infer)
        {
            alpha=0.5;
        }

        for(String s:aspect_prob.get(query).keySet())
        {
            StringTokenizer tok = new StringTokenizer(s," ");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
            }

            for(String tok1:tokens)
            {
                if(term_statistics_mapped.get(tok1)!=null)
                {
                    if (mixture.get(term_statistics_mapped.get(tok1)) == null )
                    {
                        mixture.put(term_statistics_mapped.get(tok1), lambda2 * (alpha * aspect_prob.get(query).get(s)));
                    }
                    else if (mixture.get(term_statistics_mapped.get(tok1)) != null )
                    {
                        mixture.put(term_statistics_mapped.get(tok1), mixture.get(term_statistics_mapped.get(tok1)) + lambda2 * (alpha * aspect_prob.get(query).get(s)));
                    }
                }
            }
        }

        if(infer && aspect_prob1.get(query)!=null)
        {
            for (String s : aspect_prob1.get(query).keySet())
            {
                StringTokenizer tok = new StringTokenizer(s, " ");
                ArrayList<String> tokens = new ArrayList<String>();
                while (tok.hasMoreTokens())
                {
                    tokens.add(tok.nextToken());
                }

                for (String tok1 : tokens)
                {
                    if(term_statistics_mapped.get(tok1)!=null)
                    {
                        if (mixture.get(term_statistics_mapped.get(tok1)) == null)
                        {
                            mixture.put(term_statistics_mapped.get(tok1), lambda2 * ((1 - alpha) * aspect_prob1.get(query).get(s)));
                        }
                        else if (mixture.get(term_statistics_mapped.get(tok1)) != null)
                        {
                            mixture.put(term_statistics_mapped.get(tok1), mixture.get(term_statistics_mapped.get(tok1)) + lambda2 * ((1 - alpha) * aspect_prob1.get(query).get(s)));
                        }
                    }
                }
            }
        }

        System.out.println(mixture.size());

        BufferedWriter bw1 = new BufferedWriter(new FileWriter(mixturefile+"_"+query+"_aspect_mixture_model"));
        for(int s:mixture.keySet())
        {
            bw1.write(s+"\t"+mixture.get(s)+"\n");
        }
        bw1.close();


    }


}
