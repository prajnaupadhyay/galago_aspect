package org.lemurproject.galago.core.clustering;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.lemurproject.galago.core.tools.Search.SearchResult;
import org.lemurproject.galago.core.tools.Search.SearchResultItem;

import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Emit;
import static org.lemurproject.galago.core.clustering.UtilityFunctions.calculateAverage;
import static org.lemurproject.galago.core.clustering.UtilityFunctions.cosine;
import static org.lemurproject.galago.core.clustering.UtilityFunctions.euclideanDistance;
import static org.lemurproject.galago.core.clustering.UtilityFunctions.getCHIndex;
import com.aliasi.cluster.*;
import com.aliasi.features.*;
import com.aliasi.util.Distance;
import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.*;

public class HierarchialClustering {

    public Set<Set<Pair<SearchResultItem,ArrayList<Double>>>> cluster_top_k_documents(SearchResult result) throws Exception
    {
        FileReader embeddings_file_reader = new FileReader("/home/hemanth/iitd/galago_embeddings.txt");
        FileReader mappings_file_reader = new FileReader("/home/hemanth/iitd/galago_mappings.txt");

        BufferedReader embeddings_buffered_reader = new BufferedReader(embeddings_file_reader);
        BufferedReader mappings_buffered_reader = new BufferedReader(mappings_file_reader);

        Map<Integer,ArrayList<Double>> embeddings_map = new HashMap<Integer, ArrayList<Double>>();
        Map<String,Integer> mappings_map = new HashMap<String,Integer>();
        Set<Pair<SearchResultItem,ArrayList<Double>>> docs = new HashSet<Pair<SearchResultItem,ArrayList<Double>>>();

        Integer number_of_embeddings;
        Integer size_of_embedding;

        String[] meta_data = embeddings_buffered_reader.readLine().split("\\s+");

        number_of_embeddings = Integer.parseInt(meta_data[0]);
        size_of_embedding = Integer.parseInt(meta_data[1]);

        System.out.println("The number of embeddings are "+number_of_embeddings);
        System.out.println("The size of each embedding is "+size_of_embedding);

        String current_line;

        while((current_line = embeddings_buffered_reader.readLine())!=null)
        {
            String[] embedding = current_line.split("\\s+");
            Integer key = Integer.parseInt(embedding[0]);
            ArrayList<Double> value = new ArrayList<Double>();
            for(int i=1;i<embedding.length;i++)
            {
                value.add(Double.valueOf(embedding[i]));
            }
            embeddings_map.put(key,value);
        }

        System.out.println("Embeddings Map Size is "+embeddings_map.size());

        while((current_line = mappings_buffered_reader.readLine())!=null)
        {
            String[] mapping = current_line.split("\\s+");
            Integer value = Integer.parseInt(mapping[0]);
            String key = mapping[1].replace('_',' ');
            mappings_map.put(key,value);
        }
        System.out.println(" Mappings map size is "+mappings_map.size());


        Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        for(String keyword : mappings_map.keySet())
        {
            trie_builder = trie_builder.addKeyword(keyword);
        }
        Trie trie = trie_builder.build();


        int count =0;
        for (SearchResultItem item : result.items)
        {
            count =count+1;

            System.out.println(" Parsing  Document "+count+" \n\n");
            //System.out.println("Document content is \n "+item.document.text);
            ArrayList<ArrayList<Double>> named_entities_of_item = new ArrayList<ArrayList<Double>>();
            Collection<Emit> named_entity_occurences = trie.parseText(item.document.text);

            for(Emit named_entity_occurence : named_entity_occurences)
            {
                    //System.out.println("Matched Named Entity in this document is " + named_entity_occurence.getKeyword());
                    ArrayList<Double> named_entity_embedding = embeddings_map.get(mappings_map.get(named_entity_occurence.getKeyword()));
                    if(named_entity_embedding!=null)
                    {
                        named_entities_of_item.add(named_entity_embedding);
                        System.out.println("Embedding is found for named entity "+ named_entity_occurence.getKeyword());
                    }
                    else
                    {
                        System.out.println(" There is a no matching found " + " for " +  named_entity_occurence.getKeyword());

                    }
            }

            ArrayList<Double> doc_average =  calculateAverage(named_entities_of_item);

            if(doc_average == null)
            {
                System.out.println("Document average is null");
                doc_average = new ArrayList<Double>(size_of_embedding);

                for(int i=0;i<size_of_embedding;i++)
                {
                    doc_average.add(new Double(0.000001));
                }
            }
            docs.add(new MutablePair<>(item, doc_average));
        }

        for(Pair<SearchResultItem,ArrayList<Double>> doc:docs)
        {
            for(Double item:doc.getRight())
            {
               ;// System.out.print(item+" ");
            }
            //System.out.print("\n\n");
        }
        //System.out.println(docs);

        //System.out.println(" Number of Documents  are : "+docs.size());


        final Distance<Pair<SearchResultItem,ArrayList<Double>>> COSINE_DISTANCE
                = new Distance<Pair<SearchResultItem,ArrayList<Double>>>() {
            public double distance(Pair<SearchResultItem,ArrayList<Double>> point1,
                                   Pair<SearchResultItem,ArrayList<Double>> point2)
            {
                double oneMinusCosine = 1.0 - cosine(point1.getValue(),point2.getValue());
                if (oneMinusCosine > 1.0)
                    return 1.0;
                else if (oneMinusCosine < 0.0)
                    return 0.0;
                else
                    return oneMinusCosine;
            }
        };

        HierarchicalClusterer<Pair<SearchResultItem,ArrayList<Double>>> clClusterer;
        clClusterer = new CompleteLinkClusterer<Pair<SearchResultItem,ArrayList<Double>>>(COSINE_DISTANCE);

        Dendrogram<Pair<SearchResultItem,ArrayList<Double>>> completeLinkDendrogram
                = clClusterer.hierarchicalCluster(docs);


        double max_c_h_index = 0;
        int optimum_no_of_clusters=1;
        for(int i=1;i<=docs.size();i++)
        {
            Set<Set<Pair<SearchResultItem,ArrayList<Double>>>> clusters
                    = completeLinkDendrogram.partitionK(i);
            int n=0;
            for(Set<Pair<SearchResultItem,ArrayList<Double>>> cluster:clusters)
            {
                n+=cluster.size();
            }
           // System.out.println("The total Number of points in the sample space is "+n);
            double curr_c_h_index = getCHIndex(clusters,i,n);

            if(curr_c_h_index>max_c_h_index)
            {
                max_c_h_index = curr_c_h_index;
                optimum_no_of_clusters = i;
            }

        }

        System.out.println(" The optimum number of clusters are : " + optimum_no_of_clusters);
        System.out.println(" The Max CHIndex value obtained is  : " + max_c_h_index);

        Set<Set<Pair<SearchResultItem,ArrayList<Double>>>> clusters
                = completeLinkDendrogram.partitionK(optimum_no_of_clusters);
        System.out.println(" clusters size is "+clusters.size());
        return clusters;

    }
}
