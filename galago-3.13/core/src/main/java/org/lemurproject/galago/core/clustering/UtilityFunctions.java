package org.lemurproject.galago.core.clustering;

import org.apache.commons.lang3.tuple.Pair;
import org.lemurproject.galago.core.tools.Search;

import java.util.ArrayList;
import java.util.Set;

public class UtilityFunctions {


    public static ArrayList<Double> calculateAverage(ArrayList<ArrayList<Double>> namedEntities)
    {

        if(namedEntities.isEmpty())
        {
            System.out.println(" No named entites are present in this document ");
            return null;

        }

        int size_of_named_entity = namedEntities.get(0).size();
        ArrayList<Double> namedEntities_sum = new ArrayList<Double>(size_of_named_entity);

        for(int i=0;i<size_of_named_entity;i++)
        {
            namedEntities_sum.add(new Double(0));
        }

        for(int i=0;i<size_of_named_entity;i++)
        {
            for(int j=0;j<namedEntities.size();j++)
            {
                namedEntities_sum.set(i,namedEntities_sum.get(i)+namedEntities.get(j).get(i));
            }
        }
        for(int i=0;i<size_of_named_entity;i++)
        {
            namedEntities_sum.set(i,namedEntities_sum.get(i)/namedEntities.size());
        }

        return namedEntities_sum;
    }




    public static double cosine(ArrayList<Double> point1, ArrayList<Double> point2)
    {
        double dot_product = 0;
        double point1_mod = 0;
        double point2_mod = 0;

        if(point1.size()!= point2.size())
        {
            System.out.println(" Document sizes are not matching");
            return 0;
        }

        for(int i = 0;i < point1.size();i++)
        {
            dot_product += point1.get(i)*point2.get(i);
            point1_mod +=  point1.get(i) * point1.get(i);
            point2_mod += point2.get(i) * point2.get(i);

        }

        point1_mod = java.lang.Math.sqrt(point1_mod);
        point2_mod = java.lang.Math.sqrt(point2_mod);

        double cosine_distance = dot_product/(point1_mod*point2_mod);
        return cosine_distance;

    }

    public static double euclideanDistance(ArrayList<Double> point1,ArrayList<Double> point2)
    {
        if(point1==null)
            return 10000;
        if(point2==null)
            return 10000;
        if(point1.size()!= point2.size())
        {
            System.out.println("The two arrays must be of same size to calculate euclidean distance");
            return 10000;

        }

        double result=0;

        for(int i=0;i<point1.size();i++)
        {
            result += Math.pow(point1.get(i)-point2.get(i),2);
        }

        return result;
    }

    public static double euclidean_distance(ArrayList<Double> point1,ArrayList<Double> point2)
    {
        if(point1==null)
        {
            System.out.println(" point1 is null. Hence distance cannot be caluculated");
            return Double.MAX_VALUE;
        }

        if(point2==null)
        {
            System.out.println(" point2 is null. Hence distance cannot be caluculated");
            return Double.MAX_VALUE;
        }

        if(point1.size()!= point2.size())
        {
            System.out.println("The two arrays must be of same size to calculate euclidean distance");
            return 10000;
        }

        double result=0;

        for(int i=0;i<point1.size();i++)
        {
            result += Math.pow(point1.get(i)-point2.get(i),2);
        }

        return result;
    }

    public static double getCHIndex(Set<Set<Pair<Search.SearchResultItem,ArrayList<Double>>>> cluster_set, int k, int n)
    {

        double total_intra_cluster_variation = 0;
        double total_inter_cluster_variation = 0;
        double c_h_index = 0;

        ArrayList<ArrayList<Double>> cluster_centroids = new ArrayList<ArrayList<Double>>();
        ArrayList<Double> centroid_of_centroids;

        for(Set<Pair<Search.SearchResultItem,ArrayList<Double>>> cluster:cluster_set)
        {
            ArrayList<ArrayList<Double>> cluster_points = new ArrayList<ArrayList<Double>>();

            for(Pair<Search.SearchResultItem,ArrayList<Double>> cluster_point: cluster)
            {
                cluster_points.add(cluster_point.getRight());
            }
            ArrayList<Double> cluster_centroid = calculateAverage(cluster_points);
            if(cluster_centroid!=null)
                cluster_centroids.add(cluster_centroid);
            else
                System.out.println("A cluster is generated with 0 elements");
        }

        centroid_of_centroids = calculateAverage(cluster_centroids);

        int cluster_no =0;

        for( Set<Pair<Search.SearchResultItem,ArrayList<Double>>> cluster:cluster_set)
        {

            for( Pair<Search.SearchResultItem,ArrayList<Double>> cluster_point: cluster)
            {
                cluster_point.getRight();
                total_intra_cluster_variation += euclidean_distance(cluster_point.getRight(),cluster_centroids.get(cluster_no));
            }
            ++cluster_no;
        }

        for( int i=0;i<cluster_centroids.size();i++)
        {
            total_inter_cluster_variation += euclidean_distance(cluster_centroids.get(i),centroid_of_centroids);
        }

        c_h_index = ((total_inter_cluster_variation)*(n-k))/((total_intra_cluster_variation)*(k-1));
        return c_h_index;
    }
}
