// BSD License (http://lemurproject.org/galago-license)
package org.lemurproject.galago.core.retrieval.processing;

import org.ahocorasick.trie.Emit;
import org.ahocorasick.trie.Trie;
import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.ScoreIterator;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

/**
 * Performs straightforward document-at-a-time (daat) processing of a fully
 * annotated query, processing scores over documents.
 *
 * @author irmarc, sjh
 */
public class AspectMixtureModelAdded extends ProcessingModel {

    LocalRetrieval retrieval;
    Index index;
    public HashMap<String, Double> word_prob;
    public HashMap<String, HashMap<String, Double>> entity_prob;
    public HashMap<String, Double> mixture;
    public HashMap<Long, HashMap<Integer, Double>> freq_prob;
    public HashMap<Integer, HashMap<Long, Double>> inverse_freq_probability;
    public HashMap<String, Integer> entities;
    public HashMap<Integer, String> integer_to_string;
    public String aspect_name;
    public String mixturefile;
    public String smooth;
    public String abstracts;
    public String mixturechoice;
    public double lambda1;
    public double lambda2;

    public AspectMixtureModelAdded(LocalRetrieval lr, String entities_file, String entities_index_file, String word_prob_file, String entity_prob_file, String aspect_name, double lambda1, double lambda2, String mixturefile,  String smooth, String abstracts, String mixturechoice) throws Exception {

        BufferedReader br1 = new BufferedReader(new FileReader(word_prob_file));
        BufferedReader br2 = new BufferedReader(new FileReader(entity_prob_file));
        HashMap<String, HashMap<String, Double>> aspect_scores = new HashMap<String, HashMap<String, Double>>();
        HashMap<String, Double> word_scores = new HashMap<String, Double>();
        double aspect_score_sum=0;
        String line;
        while((line=br2.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," "));
                //entities.add(to)
            }
            if(tokens.size()!=3) continue;
            HashMap<String, Double> scores;
            if(aspect_scores.get(tokens.get(0))!=null)
            {
                scores = aspect_scores.get(tokens.get(0));
            }
            else
            {
                scores = new HashMap<String, Double>();
            }
            //System.out.println(scores);
            scores.put(tokens.get(1),Double.parseDouble(tokens.get(2)));
            aspect_scores.put(tokens.get(0),scores);
            aspect_score_sum = aspect_score_sum + Double.parseDouble(tokens.get(2));
        }
        HashMap<String, HashMap<String, Double>> aspect_prob = new HashMap<String, HashMap<String, Double>>();

        for(String i:aspect_scores.keySet())
        {
            HashMap<String, Double> new_scores = new HashMap<String, Double>();
            for(String j:aspect_scores.get(i).keySet())
            {
                double sc=aspect_scores.get(i).get(j)/aspect_score_sum;
                new_scores.put(j,sc);
            }
            aspect_prob.put(i, new_scores);
        }
        double word_sum=0;
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," "));
                //entities.add(to)
            }
            word_scores.put(tokens.get(0), Double.parseDouble(tokens.get(1)));
            word_sum = word_sum + Double.parseDouble(tokens.get(1));
        }
        HashMap<String, Double> word_prob = new HashMap<String, Double>();
        for(String s:word_scores.keySet())
        {
            word_prob.put(s,word_scores.get(s)/word_sum);
        }

        br1 = new BufferedReader(new FileReader(entities_file));

        HashMap<String, Integer> entities = new HashMap<String, Integer>();
        HashMap<Integer, String> integer_to_string = new HashMap<Integer, String>();
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            entities.put(tokens.get(1).replace("_"," "),Integer.parseInt(tokens.get(0)));
            integer_to_string.put(Integer.parseInt(tokens.get(0)), tokens.get(1).replace("_"," "));
        }

        HashMap<Long, HashMap<Integer, Double>> entity_frequencies = new HashMap<Long, HashMap<Integer, Double>>();

        BufferedReader br3 = new BufferedReader(new FileReader(entities_index_file));
        while((line=br3.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            if(tokens.size()!=2) continue;
            if(entity_frequencies.get(Long.parseLong(tokens.get(1)))==null)
            {
                entity_frequencies.put(Long.parseLong(tokens.get(1)), new HashMap<Integer, Double>());
            }
            HashMap<Integer, Double> h = entity_frequencies.get(Long.parseLong(tokens.get(1)));
            if(h.get(Integer.parseInt(tokens.get(0)))==null)
            {
                h.put(Integer.parseInt(tokens.get(0)),1.0);
            }
            else
            {
                h.put(Integer.parseInt(tokens.get(0)),h.get(Integer.parseInt(tokens.get(0)))+1);
            }
            entity_frequencies.put(Long.parseLong(tokens.get(1)),h);
        }

        HashMap<Long, HashMap<Integer, Double>> freq_prob = new HashMap<Long, HashMap<Integer, Double>>();
        HashMap<Integer, HashMap<Long, Double>> inverse_freq_prob = new HashMap<>();
        for(long i:entity_frequencies.keySet())
        {
            HashMap<Integer, Double> h = new HashMap<Integer, Double>();

            double sum=0;
            for(int j:entity_frequencies.get(i).keySet())
            {
                sum = sum + entity_frequencies.get(i).get(j);
            }

            for(int j:entity_frequencies.get(i).keySet())
            {
                double prob = entity_frequencies.get(i).get(j)/sum;
                h.put(j,prob);
                HashMap<Long, Double> h1;
                if(inverse_freq_prob.get(j)==null)
                {
                    h1 = new HashMap<>();
                }
                else
                {
                    h1 = inverse_freq_prob.get(j);
                }
                h1.put(i,prob);
                inverse_freq_prob.put(j,h1);
            }
            freq_prob.put(i,h);
        }

        this.retrieval = lr;
        this.index = lr.getIndex();
        this.word_prob=word_prob;
        this.entity_prob=aspect_prob;
        this.freq_prob = freq_prob;
        this.inverse_freq_probability = inverse_freq_prob;
        this.entities = entities;
        this.integer_to_string = integer_to_string;
        this.lambda1 = lambda1;
        this.lambda2 = lambda2;
        this.aspect_name = aspect_name;
        this.mixturefile = mixturefile;
        this.smooth = smooth;
        this.abstracts = abstracts;
        this.mixturechoice=mixturechoice;


    }

    public void mixEntities(String query, String mixturefile) throws Exception
    {
        HashMap<String, Double> mixture = new HashMap<String, Double>();



        for(String s:entity_prob.get(query).keySet())
        {
            double mix_prob = lambda1*entity_prob.get(query).get(s);
            StringTokenizer tok = new StringTokenizer(s," ");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
            }

            double word_part=1.0;

            for(String tok1:tokens) {
                if (word_prob.get(tok1) == null) {
                    word_part = word_part*(1/word_prob.keySet().size());
                } else {
                    word_part = word_part*(word_prob.get(tok1));                }
            }

            mix_prob = mix_prob + lambda2*word_part;
            mixture.put(s,mix_prob);
        }

        BufferedWriter bw1 = new BufferedWriter(new FileWriter(mixturefile+"_"+query));
        for(String s:mixture.keySet())
        {
            bw1.write(s+"\t"+mixture.get(s)+"\n");
        }
        bw1.close();

        this.mixture = mixture;

    }

    public void mix(String query, String mixturefile) throws Exception
    {
        HashMap<String, Double> mixture = new HashMap<String, Double>();

        for(String s:word_prob.keySet())
        {
            mixture.put(s,lambda1*word_prob.get(s));
        }

        for(String s:entity_prob.get(query).keySet())
        {
            StringTokenizer tok = new StringTokenizer(s," ");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
            }

            for(String tok1:tokens)
            {
                if (mixture.get(tok1) == null)
                {
                    mixture.put(tok1, lambda2 * entity_prob.get(query).get(s));
                } else {
                    mixture.put(tok1, mixture.get(tok1) + lambda2 * entity_prob.get(query).get(s));
                }
            }
        }

        BufferedWriter bw1 = new BufferedWriter(new FileWriter(mixturefile+"_"+query));
        for(String s:mixture.keySet())
        {
            bw1.write(s+"\t"+mixture.get(s)+"\n");
        }
        bw1.close();

        this.mixture = mixture;

    }

    public double scoreEntities(Long document, org.lemurproject.galago.core.parse.Document.DocumentComponents p1, org.lemurproject.galago.core.tools.Search s) throws Exception
    {
        double score=0;
        String docname = index.getName(document);
        org.lemurproject.galago.core.parse.Document d = s.getDocument(docname, p1);
   /* StringTokenizer tok = new StringTokenizer(d.text, " ");
    while (tok.hasMoreTokens()) {
      String token = tok.nextToken();
      if (word_prob.get(token) != null) {
        score = score + Math.log(word_prob.get(token));
        // number = number+1;
      } else {
        double smooth = Math.log(1.0 / word_prob.keySet().size());
        score = score + smooth;
      }

    }*/
        return score;

    }


    @Override
    public ScoredDocument[] execute(Node queryTree, Parameters queryParams) throws Exception {
        // This model uses the simplest ScoringContext
        ScoringContext context = new ScoringContext();
        String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();;
        System.out.println("inside aspectmixturemodeladded, printing the query_text: "+query_text);
        // Number of documents requested.
        int requested = queryParams.get("requested", 1000);
        boolean annotate = queryParams.get("annotate", false);

        // Maintain a queue of candidates
        FixedSizeMinHeap<ScoredDocument> queue = new FixedSizeMinHeap<>(ScoredDocument.class, requested, new ScoredDocument.ScoredDocumentComparator());
   /* Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
    for(String keyword : entities.keySet())
    {
      trie_builder = trie_builder.addKeyword(keyword);
    }
    Trie trie1 = trie_builder.build();*/
        boolean flag1=false;
        if(entity_prob.get(query_text)==null)
        {
            System.out.println("this aspect has no embedding for the given query "+query_text);
            flag1=true;
        }
        else
        {
            if(mixturechoice.equals("entities")) {
                mixEntities(query_text, this.mixturefile);
            }
            else if(mixturechoice.equals("words"))
            {
                mix(query_text, this.mixturefile);
            }
        }
        // construct the iterators -- we use tree processing
        ScoreIterator iterator = (ScoreIterator) retrieval.createIterator(queryParams, queryTree);
        ScoreIterator iterator1 = (ScoreIterator) retrieval.createIterator(queryParams, queryTree);
        int cc1=0;
        while(!iterator1.isDone())
        {
            cc1++;
            long document = iterator1.currentCandidate();
            iterator1.syncTo(document);
            iterator1.movePast(document);
        }

        System.out.print("size of the iterator: "+cc1);

        org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
        org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(queryParams);

        int cc=0;
        // now there should be an iterator at the root of this tree
        while (!iterator.isDone()) {
            long document = iterator.currentCandidate();
              //System.out.println(cc);
          cc++;
          if(cc % 10000==0)
          {
            System.out.println(cc);
          }
            // This context is shared among all scorers
            context.document = document;
            iterator.syncTo(document);
            double score1=0.0;
            if (iterator.hasMatch(context))
            {
                score1 = iterator.score(context);
                // score = score + 10;
                int number = 0;
                double score = 0.0;
                if(!flag1) {
                    String docname = index.getName(document);
                    org.lemurproject.galago.core.parse.Document d = s.getDocument(docname, p1);
                    if(abstracts.equals("yes")) {
                        if (d.contentForXMLTag("title").equals("") || d.contentForXMLTag("paperAbstract").equals(""))
                            continue;
                    }
                    StringTokenizer tok = new StringTokenizer(d.contentForXMLTag("title") + " " + d.contentForXMLTag("paperAbstract").toLowerCase(), " ");
                    while (tok.hasMoreTokens()) {
                        String token = tok.nextToken();
                        if (mixture.get(token) != null) {
                            score = score + Math.log(mixture.get(token));
                            // number = number+1;
                        }
                        else {
                            if(smooth.equals("yes")) {
                                double smooth = Math.log(1.0 / mixture.keySet().size());
                                score = score + smooth;
                            }
                        }

                    }
                }
      /*  if(!flag)
        {

          score = scoreEntities(document, p1, s);

        }*/


                double totalscore = score1 + score;

                if (queue.size() < requested || queue.peek().score < totalscore)
                {
                    ScoredDocument scoredDocument = new ScoredDocument(document, totalscore);
                    if (annotate)
                    {
                        scoredDocument.annotation = iterator.getAnnotatedNode(context);
                    }
                    queue.offer(scoredDocument);
                }
            }
            iterator.movePast(document);
        }
        return toReversedArray(queue);
    }
}
