package org.lemurproject.galago.core.retrieval.processing;

import org.ahocorasick.trie.Emit;
import org.ahocorasick.trie.Trie;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.io.*;
import java.util.*;

/**
 * class representing a mixture model to describe the relevance of the documents. The probabilities of the words are mixed with the probabilities of the entities.
 */

public class AspectMixtureModelEntities extends org.lemurproject.galago.core.retrieval.processing.ProcessingModel
{
    org.lemurproject.galago.core.retrieval.LocalRetrieval retrieval;
    org.lemurproject.galago.core.index.Index index;
    public HashMap<String, Double> word_prob;
    public HashMap<String, HashMap<String, Double>> entity_prob;
    public HashMap<String, Double> mixture;
    public HashMap<Long, HashMap<Integer, Double>> freq_prob;
    public HashMap<Integer, HashMap<Long, Double>> inverse_freq_probability;
    public HashMap<String, Integer> entities;
    public HashMap<Integer, String> integer_to_string;
    public String aspect_name;
    public String mixturefile;
    public String smooth;
    public String abstracts;
    public double lambda1;
    public double lambda2;

    public AspectMixtureModelEntities(org.lemurproject.galago.core.retrieval.LocalRetrieval lr, String entities_file, String entities_index_file, String word_prob_file, String entity_prob_file, String aspect_name, double lambda1, double lambda2, String mixturefile, String smooth, String abstracts) throws Exception
    {
        BufferedReader br1 = new BufferedReader(new FileReader(word_prob_file));
        BufferedReader br2 = new BufferedReader(new FileReader(entity_prob_file));
        HashMap<String, HashMap<String, Double>> aspect_scores = new HashMap<String, HashMap<String, Double>>();
        HashMap<String, Double> word_scores = new HashMap<String, Double>();


        double aspect_score_sum=0;
        String line;
        while((line=br2.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," ").replace("-"," "));
                //entities.add(to)
            }
            if(tokens.size()!=3) continue;
            HashMap<String, Double> scores;
            if(aspect_scores.get(tokens.get(0))!=null)
            {
                scores = aspect_scores.get(tokens.get(0));
            }
            else
            {
                scores = new HashMap<String, Double>();
            }
            //System.out.println(scores);
            scores.put(tokens.get(1),Double.parseDouble(tokens.get(2)));
            aspect_scores.put(tokens.get(0),scores);
            aspect_score_sum = aspect_score_sum + Double.parseDouble(tokens.get(2));
        }
        HashMap<String, HashMap<String, Double>> aspect_prob = new HashMap<String, HashMap<String, Double>>();

        for(String i:aspect_scores.keySet())
        {
            HashMap<String, Double> new_scores = new HashMap<String, Double>();
            for(String j:aspect_scores.get(i).keySet())
            {
                double sc=aspect_scores.get(i).get(j)/aspect_score_sum;
                new_scores.put(j,sc);
            }
            aspect_prob.put(i, new_scores);
        }
        double word_sum=0;
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," "));
                //entities.add(to)
            }
            word_scores.put(tokens.get(0), Double.parseDouble(tokens.get(1)));
            word_sum = word_sum + Double.parseDouble(tokens.get(1));
        }
        HashMap<String, Double> word_prob = new HashMap<String, Double>();
        for(String s:word_scores.keySet())
        {
            word_prob.put(s,word_scores.get(s)/word_sum);
        }

        br1 = new BufferedReader(new FileReader(entities_file));

        HashMap<String, Integer> entities = new HashMap<String, Integer>();
        HashMap<Integer, String> integer_to_string = new HashMap<Integer, String>();
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            entities.put(tokens.get(1).replace("_"," ").replace("-"," "),Integer.parseInt(tokens.get(0)));
            integer_to_string.put(Integer.parseInt(tokens.get(0)), tokens.get(1).replace("_"," ").replace("-"," "));
        }

        HashMap<Long, HashMap<Integer, Double>> entity_frequencies = new HashMap<Long, HashMap<Integer, Double>>();

        BufferedReader br3 = new BufferedReader(new FileReader(entities_index_file));
        while((line=br3.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            if(tokens.size()!=2) continue;
            if(entity_frequencies.get(Long.parseLong(tokens.get(1)))==null)
            {
                entity_frequencies.put(Long.parseLong(tokens.get(1)), new HashMap<Integer, Double>());
            }
            HashMap<Integer, Double> h = entity_frequencies.get(Long.parseLong(tokens.get(1)));
            if(h.get(Integer.parseInt(tokens.get(0)))==null)
            {
                h.put(Integer.parseInt(tokens.get(0)),1.0);
            }
            else
            {
                h.put(Integer.parseInt(tokens.get(0)),h.get(Integer.parseInt(tokens.get(0)))+1);
            }
            entity_frequencies.put(Long.parseLong(tokens.get(1)),h);
        }

        HashMap<Long, HashMap<Integer, Double>> freq_prob = new HashMap<Long, HashMap<Integer, Double>>();
        HashMap<Integer, HashMap<Long, Double>> inverse_freq_prob = new HashMap<>();
        for(long i:entity_frequencies.keySet())
        {
            HashMap<Integer, Double> h = new HashMap<Integer, Double>();

            double sum=0;
            for(int j:entity_frequencies.get(i).keySet())
            {
                sum = sum + entity_frequencies.get(i).get(j);
            }

            for(int j:entity_frequencies.get(i).keySet())
            {
                double prob = entity_frequencies.get(i).get(j)/sum;
                h.put(j,prob);
                HashMap<Long, Double> h1;
                if(inverse_freq_prob.get(j)==null)
                {
                    h1 = new HashMap<>();
                }
                else
                {
                    h1 = inverse_freq_prob.get(j);
                }
                h1.put(i,prob);
                inverse_freq_prob.put(j,h1);
            }
            freq_prob.put(i,h);
        }

        this.retrieval = lr;
        this.index = lr.getIndex();
        this.word_prob=word_prob;
        this.entity_prob=aspect_prob;
        this.freq_prob = freq_prob;
        this.inverse_freq_probability = inverse_freq_prob;
        this.entities = entities;
        this.integer_to_string = integer_to_string;
        this.lambda1 = lambda1;
        this.lambda2 = lambda2;
        this.aspect_name = aspect_name;
        this.mixturefile = mixturefile;
        this.smooth = smooth;
        this.abstracts = abstracts;

    }

    public AspectMixtureModelEntities( String entities_file, String entities_index_file, String word_prob_file, String entity_prob_file, String aspect_name, double lambda1, double lambda2, String mixturefile, String smooth, String abstracts) throws Exception
    {
        BufferedReader br1 = new BufferedReader(new FileReader(word_prob_file));
        BufferedReader br2 = new BufferedReader(new FileReader(entity_prob_file));
        HashMap<String, HashMap<String, Double>> aspect_scores = new HashMap<String, HashMap<String, Double>>();
        HashMap<String, Double> word_scores = new HashMap<String, Double>();


        double aspect_score_sum=0;
        String line;
        while((line=br2.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            int cc=0;
            while(tok.hasMoreTokens())
            {
                if(cc!=2)
                    tokens.add(tok.nextToken().replace("_"," ").replace("-"," "));
                else
                {
                    tokens.add(tok.nextToken());
                }
                cc++;
                //entities.add(to)
            }
            if(tokens.size()!=3) continue;

            HashMap<String, Double> scores;
            if(aspect_scores.get(tokens.get(0))!=null)
            {
                scores = aspect_scores.get(tokens.get(0));
            }
            else
            {
                scores = new HashMap<String, Double>();
            }
            //System.out.println(scores);
            scores.put(tokens.get(1),Double.parseDouble(tokens.get(2)));
            aspect_scores.put(tokens.get(0),scores);
            aspect_score_sum = aspect_score_sum + Double.parseDouble(tokens.get(2));
        }
        HashMap<String, HashMap<String, Double>> aspect_prob = new HashMap<String, HashMap<String, Double>>();

        for(String i:aspect_scores.keySet())
        {
            HashMap<String, Double> new_scores = new HashMap<String, Double>();
            for(String j:aspect_scores.get(i).keySet())
            {
                double sc=aspect_scores.get(i).get(j)/aspect_score_sum;
                new_scores.put(j,sc);
            }
            aspect_prob.put(i, new_scores);
        }
        double word_sum=0;
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," "));
                //entities.add(to)
            }
            word_scores.put(tokens.get(0), Double.parseDouble(tokens.get(1)));
            word_sum = word_sum + Double.parseDouble(tokens.get(1));
        }
        HashMap<String, Double> word_prob = new HashMap<String, Double>();
        for(String s:word_scores.keySet())
        {
            word_prob.put(s,word_scores.get(s)/word_sum);
        }

        br1 = new BufferedReader(new FileReader(entities_file));

        HashMap<String, Integer> entities = new HashMap<String, Integer>();
        HashMap<Integer, String> integer_to_string = new HashMap<Integer, String>();
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            if(tokens.size()!=2) continue;
            entities.put(tokens.get(1).replace("_"," ").replace("-"," "),Integer.parseInt(tokens.get(0)));
            integer_to_string.put(Integer.parseInt(tokens.get(0)), tokens.get(1).replace("_"," ").replace("-"," "));
        }

        HashMap<Long, HashMap<Integer, Double>> entity_frequencies = new HashMap<Long, HashMap<Integer, Double>>();

        BufferedReader br3 = new BufferedReader(new FileReader(entities_index_file));
        while((line=br3.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            if(tokens.size()!=2) continue;
            if(entity_frequencies.get(Long.parseLong(tokens.get(1)))==null)
            {
                entity_frequencies.put(Long.parseLong(tokens.get(1)), new HashMap<Integer, Double>());
            }
            HashMap<Integer, Double> h = entity_frequencies.get(Long.parseLong(tokens.get(1)));
            if(h.get(Integer.parseInt(tokens.get(0)))==null)
            {
                h.put(Integer.parseInt(tokens.get(0)),1.0);
            }
            else
            {
                h.put(Integer.parseInt(tokens.get(0)),h.get(Integer.parseInt(tokens.get(0)))+1);
            }
            entity_frequencies.put(Long.parseLong(tokens.get(1)),h);
        }

        HashMap<Long, HashMap<Integer, Double>> freq_prob = new HashMap<Long, HashMap<Integer, Double>>();
        HashMap<Integer, HashMap<Long, Double>> inverse_freq_prob = new HashMap<>();
        for(long i:entity_frequencies.keySet())
        {
            HashMap<Integer, Double> h = new HashMap<Integer, Double>();

            double sum=0;
            for(int j:entity_frequencies.get(i).keySet())
            {
                sum = sum + entity_frequencies.get(i).get(j);
            }

            for(int j:entity_frequencies.get(i).keySet())
            {
                double prob = entity_frequencies.get(i).get(j)/sum;
                h.put(j,prob);
                HashMap<Long, Double> h1;
                if(inverse_freq_prob.get(j)==null)
                {
                    h1 = new HashMap<>();
                }
                else
                {
                    h1 = inverse_freq_prob.get(j);
                }
                h1.put(i,prob);
                inverse_freq_prob.put(j,h1);
            }
            freq_prob.put(i,h);
        }


        this.word_prob=word_prob;
        this.entity_prob=aspect_prob;
        this.freq_prob = freq_prob;
        this.inverse_freq_probability = inverse_freq_prob;
        this.entities = entities;
        this.integer_to_string = integer_to_string;
        this.lambda1 = lambda1;
        this.lambda2 = lambda2;
        this.aspect_name = aspect_name;
        this.mixturefile = mixturefile;
        this.smooth = smooth;
        this.abstracts = abstracts;

    }

    public void mix(String query, String mixturefile) throws Exception
    {
        HashMap<String, Double> mixture = new HashMap<String, Double>();



        for(String s:entity_prob.get(query).keySet())
        {
            double mix_prob = lambda1*entity_prob.get(query).get(s);
            StringTokenizer tok = new StringTokenizer(s," ");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
            }

            double word_part=1.0;

            for(String tok1:tokens) {
                if (word_prob.get(tok1) == null) {
                    word_part = word_part*(1/word_prob.keySet().size());
                } else {
                    word_part = word_part*(word_prob.get(tok1));                }
            }

            mix_prob = mix_prob + lambda2*word_part;
            mixture.put(s,mix_prob);
        }

        BufferedWriter bw1 = new BufferedWriter(new FileWriter(mixturefile+"_"+query));
        for(String s:mixture.keySet())
        {
            bw1.write(s+"\t"+mixture.get(s)+"\n");
        }
        bw1.close();

        this.mixture = mixture;

    }


    public org.lemurproject.galago.core.retrieval.ScoredDocument[] execute(org.lemurproject.galago.core.retrieval.query.Node queryTree, org.lemurproject.galago.utility.Parameters queryParams) throws Exception
    {
        int requested = queryParams.get("requested", 1000);
        boolean annotate = queryParams.get("annotate", false);

        org.lemurproject.galago.core.retrieval.processing.ScoringContext context = new org.lemurproject.galago.core.retrieval.processing.ScoringContext();
        String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();
        System.out.println(query_text);
        org.lemurproject.galago.utility.FixedSizeMinHeap<org.lemurproject.galago.core.retrieval.ScoredDocument> queue = new org.lemurproject.galago.utility.FixedSizeMinHeap<>(org.lemurproject.galago.core.retrieval.ScoredDocument.class, requested, new org.lemurproject.galago.core.retrieval.ScoredDocument.ScoredDocumentComparator());

        Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        for(String keyword : entities.keySet())
        {
            trie_builder = trie_builder.addKeyword(keyword);
        }
        Trie trie1 = trie_builder.build();
        if(entity_prob.get(query_text)==null)
        {
            System.out.println("this aspect has no embedding for the given query "+query_text);
            return toReversedArray(queue);
        }
        mix(query_text, this.mixturefile);

        // Number of documents requested.


        // Maintain a queue of candidates

        // construct the iterators -- we use tree processing
        org.lemurproject.galago.core.retrieval.iterator.IndicatorIterator iterator =
                (org.lemurproject.galago.core.retrieval.iterator.IndicatorIterator) retrieval.createIterator(queryParams,
                        queryTree);
        HashSet<Long> documents = new HashSet<Long>();
        Collection<Emit> named_entity_occurences = trie1.parseText(query_text);
        Collection<Emit> retained_entities = new ArrayList<Emit>();
        HashSet<String> other_entities = new HashSet<String>();
        boolean flag=false;
        for (Emit named_entity_occurence : named_entity_occurences)
        {
            if(named_entity_occurence.getKeyword().equals(query_text.trim()))
            {
                int entity = entities.get(named_entity_occurence.getKeyword());
                if (inverse_freq_probability.get(entity) != null)
                {
                    documents.addAll(inverse_freq_probability.get(entity).keySet());
                    retained_entities.add(named_entity_occurence);
                }
                flag=true;

            }
            else
            {
                other_entities.add(named_entity_occurence.getKeyword());
            }


        }
        if(!flag)
        {
            for (Emit named_entity_occurence : named_entity_occurences)
            {
                retained_entities.add(named_entity_occurence);
            }
        }
        //org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
       // org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(queryParams);

        // now there should be an iterator at the root of this tree
        for(Long document:documents)
        {
            // long document = iterator.currentCandidate();
            // String name= iterator.data(context);
            // System.out.println("here is the name: "+name);
            // This context is shared among all scorers
            double number = 0;
            double score=0.0;
            if(freq_prob.get(document)!=null)
            {
                for (Emit named_entity_occurence : retained_entities)
                {

                    int entity = entities.get(named_entity_occurence.getKeyword());
                    if(freq_prob.get(document).get(entity)!=null)
                    {
                        for(int j:freq_prob.get(document).keySet())
                        {
                            if(integer_to_string.get(j)!=null)
                            {
                                String ename = integer_to_string.get(j);
                                if(other_entities.contains(ename)) continue;
                                if(mixture.get(ename)!=null)
                                {
                                    //System.out.println(ename);
                                    //score = score+Math.log(mixture.get(ename));
                                    score = score + mixture.get(ename);
                                    number = number + 1;
                                }
                                else
                                {
                                    if(smooth.equals("yes"))
                                    {
                                        double smooth = Math.log(1.0 / mixture.keySet().size());
                                        score = score + smooth;
                                        number = number+1;
                                    }
                                }

                            }
                        }
                        /*String docname = retrieval.getDocumentName(document);
                        org.lemurproject.galago.core.parse.Document d = s.getDocument(docname, p1);
                        if(d.contentForXMLTag("title").equals("") || d.contentForXMLTag("paperAbstract").equals("")) continue;

                        StringTokenizer tok = new StringTokenizer(d.contentForXMLTag("title")+" "+d.contentForXMLTag("paperAbstract").toLowerCase(), " ");
                        while(tok.hasMoreTokens())
                        {
                            String token = tok.nextToken();
                            if(mixture.get(token)!=null)
                            {
                                score = score+Math.log(mixture.get(token));
                                // number = number+1;
                            }
                            else
                            {
                                double smooth = Math.log(1.0/mixture.keySet().size());
                                score = score+smooth;
                            }

                        }*/
                    }

                }

             //   score = score + Math.log(freq_prob.get(document).keySet().size());


                if ((queue.size() < requested || queue.peek().score < score)) {
                    org.lemurproject.galago.core.retrieval.ScoredDocument scoredDocument = new org.lemurproject.galago.core.retrieval.ScoredDocument(document, number);
                    if (annotate)
                    {
                        scoredDocument.annotation = iterator.getAnnotatedNode(context);
                    }
                    queue.offer(scoredDocument);
                }
            }


        }
        return toReversedArray(queue);
    }




}