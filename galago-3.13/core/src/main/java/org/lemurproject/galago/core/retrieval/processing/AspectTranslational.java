package org.lemurproject.galago.core.retrieval.processing;

import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.ScoreIterator;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.io.*;
import java.util.*;

import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Emit;

/**
 * translational model for aspect-based retrieval
 */

public class AspectTranslational extends org.lemurproject.galago.core.retrieval.processing.ProcessingModel
{
    org.lemurproject.galago.core.retrieval.LocalRetrieval retrieval;
    org.lemurproject.galago.core.index.Index index;
    HashMap<Integer,HashMap<Integer, Double>> aspect_prob;
    HashMap<Long, HashMap<Integer, Double>> freq_prob;
    HashMap<Integer, HashMap<Long, Double>> inverse_freq_probability;
    HashMap<String, Integer> entities;
    HashMap<Integer, String> id_to_entities;
    String aspect_name;
    public AspectTranslational(org.lemurproject.galago.core.retrieval.LocalRetrieval lr, String aspect_prob_file, String entities_index_file, String aspect_name, String entities_file) throws Exception
    {

        BufferedReader br1 = new BufferedReader(new FileReader(entities_file));
        String line;
        HashMap<String, Integer> entities = new HashMap<String, Integer>();
        HashMap<Integer, String> id_to_entities1 = new HashMap<Integer, String>();
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            entities.put(tokens.get(1).replace("_"," "),Integer.parseInt(tokens.get(0)));
            id_to_entities1.put(Integer.parseInt(tokens.get(0)), tokens.get(1).replace("_"," "));
        }

        BufferedReader br2 = new BufferedReader(new FileReader(aspect_prob_file));
        HashMap<Integer, HashMap<Integer, Double>> aspect_scores = new HashMap<Integer, HashMap<Integer, Double>>();
        HashMap<Long, HashMap<Integer, Double>> entity_frequencies = new HashMap<Long, HashMap<Integer, Double>>();

        double aspect_score_sum=0;
        while((line=br2.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," "));
                //entities.add(to)
            }
            if(entities.get(tokens.get(0))!=null && entities.get(tokens.get(1))!=null)
            {
                HashMap<Integer, Double> scores;
                if(aspect_scores.get(entities.get(tokens.get(0)))!=null)
                {
                    scores = aspect_scores.get(entities.get(tokens.get(0)));

                }
                else
                {
                    scores = new HashMap<Integer, Double>();
                }
                //System.out.println(scores);
                scores.put(entities.get(tokens.get(1)),Double.parseDouble(tokens.get(2)));
                aspect_scores.put(entities.get(tokens.get(0)),scores);
                aspect_score_sum = aspect_score_sum + Double.parseDouble(tokens.get(2));
            }
        }
        BufferedReader br3 = new BufferedReader(new FileReader(entities_index_file));
        while((line=br3.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            if(tokens.size()!=2) continue;
            if(entity_frequencies.get(Long.parseLong(tokens.get(1)))==null)
            {
                entity_frequencies.put(Long.parseLong(tokens.get(1)), new HashMap<Integer, Double>());
            }
            HashMap<Integer, Double> h = entity_frequencies.get(Long.parseLong(tokens.get(1)));
            if(h.get(Integer.parseInt(tokens.get(0)))==null)
            {
                h.put(Integer.parseInt(tokens.get(0)),1.0);
            }
            else
            {
                h.put(Integer.parseInt(tokens.get(0)),h.get(Integer.parseInt(tokens.get(0)))+1);
            }
            entity_frequencies.put(Long.parseLong(tokens.get(1)),h);
        }

        HashMap<Integer, HashMap<Integer, Double>> aspect_prob = new HashMap<Integer, HashMap<Integer, Double>>();

        for(int i:aspect_scores.keySet())
        {
            HashMap<Integer, Double> new_scores = new HashMap<Integer, Double>();
            for(int j:aspect_scores.get(i).keySet())
            {
                double sc=aspect_scores.get(i).get(j)/aspect_score_sum;
                new_scores.put(j,sc);
            }
            aspect_prob.put(i, new_scores);
        }
        HashMap<Long, HashMap<Integer, Double>> freq_prob = new HashMap<Long, HashMap<Integer, Double>>();
        HashMap<Integer, HashMap<Long, Double>> inverse_freq_prob = new HashMap<>();
        for(long i:entity_frequencies.keySet())
        {
            HashMap<Integer, Double> h = new HashMap<Integer, Double>();

            double sum=0;
            for(int j:entity_frequencies.get(i).keySet())
            {
                sum = sum + entity_frequencies.get(i).get(j);
            }

            for(int j:entity_frequencies.get(i).keySet())
            {
                double prob = entity_frequencies.get(i).get(j)/sum;
                h.put(j,prob);
                HashMap<Long, Double> h1;
                if(inverse_freq_prob.get(j)==null)
                {
                    h1 = new HashMap<>();
                }
                else
                {
                    h1 = inverse_freq_prob.get(j);
                }
                h1.put(i,prob);
                inverse_freq_prob.put(j,h1);
            }
            freq_prob.put(i,h);
        }
        retrieval = lr;
        this.index = retrieval.getIndex();
        this.aspect_name = aspect_name;
        this.entities = entities;
        this.id_to_entities = id_to_entities1;
        this.aspect_prob = aspect_prob;
        this.freq_prob = freq_prob;
        this.inverse_freq_probability = inverse_freq_prob;

    }

    public org.lemurproject.galago.core.retrieval.ScoredDocument[] execute(org.lemurproject.galago.core.retrieval.query.Node queryTree, org.lemurproject.galago.utility.Parameters queryParams) throws Exception {
        // This model uses the simplest ScoringContext
        org.lemurproject.galago.core.retrieval.processing.ScoringContext context = new org.lemurproject.galago.core.retrieval.processing.ScoringContext();
        String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();
        System.out.println(query_text);
        Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        for(String keyword : entities.keySet())
        {
            trie_builder = trie_builder.addKeyword(keyword);
        }
        Trie trie1 = trie_builder.build();

        // Number of documents requested.
        int requested = queryParams.get("requested", 1000);
        boolean annotate = queryParams.get("annotate", false);

        // Maintain a queue of candidates
        org.lemurproject.galago.utility.FixedSizeMinHeap<org.lemurproject.galago.core.retrieval.ScoredDocument> queue = new org.lemurproject.galago.utility.FixedSizeMinHeap<>(org.lemurproject.galago.core.retrieval.ScoredDocument.class, requested, new org.lemurproject.galago.core.retrieval.ScoredDocument.ScoredDocumentComparator());

        // construct the iterators -- we use tree processing
       // org.lemurproject.galago.core.retrieval.iterator.IndicatorIterator iterator = (org.lemurproject.galago.core.retrieval.iterator.IndicatorIterator) retrieval.createIterator(queryParams,queryTree);

        Collection<Emit> named_entity_occurences = trie1.parseText(query_text);
        System.out.println(named_entity_occurences.size());
        HashSet<Long> documents = new HashSet<Long>();
        ArrayList<Integer> entities_retained= new ArrayList<Integer>();
        for (Emit named_entity_occurence : named_entity_occurences)
        {

            if(named_entity_occurence.getKeyword().equals(query_text.trim()))
            {

                int entity = entities.get(named_entity_occurence.getKeyword());
                entities_retained.add(entity);
                if (inverse_freq_probability.get(entity) != null)
                {
                    documents.addAll(inverse_freq_probability.get(entity).keySet());
                }
            }

        }
        System.out.println("entities retained size: "+entities_retained.size());
        // now there should be an iterator at the root of this tree
        for(Long document:documents)
        {
           // long document = iterator.currentCandidate();
            // String name= iterator.data(context);
            // System.out.println("here is the name: "+name);
            // This context is shared among all scorers
            context.document = document;
            //iterator.syncTo(document);

                double final_score=1.0;
                if(freq_prob.get(document)!=null)
                {
                   // System.out.println(document+" "+freq_prob.get(document).keySet().size());
                    int number=0;

                    for (int entity : entities_retained)
                    {
                        double score=0.0;
                       // int entity = entities.get(named_entity_occurence.getKeyword());
                        if(aspect_prob.get(entity)!=null)
                        {
                            //System.out.println("here");
                            for (int j : freq_prob.get(document).keySet())
                            {
                               // System.out.println(id_to_entities.get(j)+"\t"+aspect_prob.get(entity).get(j));
                                if (aspect_prob.get(entity).get(j) != null && freq_prob.get(document).get(j)!=null)
                                {
                                    score = score + (aspect_prob.get(entity).get(j)*freq_prob.get(document).get(j));
                                    //System.out.println(score);
                                    number++;
                                }
                            }
                        }
                        else
                        {
                            for (int j : freq_prob.get(document).keySet())
                            {
                               // System.out.println(id_to_entities.get(j));
                                if (aspect_prob.get(j) != null)
                                {
                                    if(aspect_prob.get(j).get(entity) != null)
                                    {
                                        score = score + (aspect_prob.get(j).get(entity)*freq_prob.get(document).get(j));
                                        number++;
                                    }

                                }
                            }
                        }
                       // System.out.println("number="+number);
                        //System.out.println(named_entity_occurence+": "+final_score);
                        final_score = final_score*score;


                    }
                    //System.out.println(document+" "+final_score);
                    /*if(freq_prob.get(document).keySet().size()==0)
                    {
                        final_score =0;
                    }
                    else
                        {
                        final_score = final_score / freq_prob.get(document).keySet().size();

                    }*/

                    if ((queue.size() < requested || queue.peek().score < final_score))
                    {
                        org.lemurproject.galago.core.retrieval.ScoredDocument scoredDocument = new org.lemurproject.galago.core.retrieval.ScoredDocument(document, final_score);
                        /*if (annotate) {
                            scoredDocument.annotation = iterator.getAnnotatedNode(context);
                        }*/
                        queue.offer(scoredDocument);
                    }
                }

        }
        return toReversedArray(queue);
    }

}