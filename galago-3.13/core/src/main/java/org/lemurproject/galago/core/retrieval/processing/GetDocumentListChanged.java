package org.lemurproject.galago.core.retrieval.processing;

import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.IndicatorIterator;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashSet;

public class GetDocumentListChanged extends ProcessingModel
{
    LocalRetrieval retrieval;
    Index index;



    //static ArrayList<String> tags = new ArrayList<String>({"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>", "<document>", "</document>", "<item>", "</item>"});

    public GetDocumentListChanged(LocalRetrieval lr) throws Exception
    {
        retrieval = lr;
        this.index = retrieval.getIndex();

    }

    public ScoredDocument[] execute(Node queryTree, Parameters queryParams) throws Exception
    {
       // System.out.println("this is the query tree: "+queryTree.isText());

        // This model uses the simplest ScoringContext
        ScoringContext context = new ScoringContext();

        org.lemurproject.galago.core.retrieval.iterator.scoring.DirichletScoringIterator iterator1 = (org.lemurproject.galago.core.retrieval.iterator.scoring.DirichletScoringIterator) retrieval.createIterator(queryParams, queryTree);


        int cc=0;
        ArrayList<ScoredDocument> scoredlist = new ArrayList<ScoredDocument>();
        String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();
        BufferedWriter bw = new BufferedWriter(new FileWriter("research_papers_indexed/query_wise_test_set/"+query_text.replace(" ","_")));
        BufferedWriter bw1 = new BufferedWriter(new FileWriter("research_papers_indexed/query_wise_test_set/"+query_text.replace(" ","_")+"_scores"));

        org.lemurproject.galago.utility.FixedSizeMinHeap<org.lemurproject.galago.core.retrieval.ScoredDocument> queue1 = new org.lemurproject.galago.utility.FixedSizeMinHeap<>(org.lemurproject.galago.core.retrieval.ScoredDocument.class, 5000, new org.lemurproject.galago.core.retrieval.ScoredDocument.ScoredDocumentComparator());



        while(!iterator1.isDone())
        {
            long document = iterator1.currentCandidate();
            context.document = document;
            iterator1.syncTo(document);
            if (iterator1.hasMatch(context))
            {
                double score = iterator1.score(context);

                if (queue1.size() < 1000 || queue1.peek().score < score)
                {
                    ScoredDocument scoredDocument = new ScoredDocument(document, score);

                    queue1.offer(scoredDocument);
                }
            }
            iterator1.movePast(document);
        }

        org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
        org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(queryParams);

        bw.write("id\tapplication\talgorithm\timplementation\treview\n");
        int count1=0;
        for(ScoredDocument d:toReversedArray(queue1))
        {
            count1++;
            if(count1>1000) break;
            String docname = retrieval.getDocumentName(d.document);
            org.lemurproject.galago.core.parse.Document d1 = s.getDocument(docname, p1);
            String dtext = d1.contentForXMLTag("title")+" "+d1.contentForXMLTag("paperAbstract");
            bw.write(d.document+"\t1\t1\t1\t"+dtext.replace("\n"," ").replace("\t"," ")+"\n");
            bw1.write(d.document+"\t"+d.score+"\n");
        }

        bw.close();
        bw1.close();
        //System.out.print("size of the results = "+cc);
           return scoredlist.toArray(new ScoredDocument[0]);
    }



}
