package org.lemurproject.galago.core.retrieval.processing;

import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.*;

public class GetDocumentListEvaluation extends ProcessingModel
{
    LocalRetrieval retrieval;
    Index index;



    //static ArrayList<String> tags = new ArrayList<String>({"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>", "<document>", "</document>", "<item>", "</item>"});

    public GetDocumentListEvaluation(LocalRetrieval lr) throws Exception
    {
        retrieval = lr;
        this.index = retrieval.getIndex();

    }

    public ScoredDocument[] execute(Node queryTree, Parameters queryParams) throws Exception
    {
       // System.out.println("this is the query tree: "+queryTree.isText());

        // This model uses the simplest ScoringContext
        ScoringContext context = new ScoringContext();

        org.lemurproject.galago.core.retrieval.iterator.scoring.DirichletScoringIterator iterator1 = (org.lemurproject.galago.core.retrieval.iterator.scoring.DirichletScoringIterator) retrieval.createIterator(queryParams, queryTree);


        int cc=0;
        ArrayList<ScoredDocument> scoredlist = new ArrayList<ScoredDocument>();
        String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();
        BufferedReader br = new BufferedReader(new FileReader("research_papers_indexed/query_wise_papers_top_5/"+query_text.replace(" ","_")));
        BufferedWriter bw = new BufferedWriter(new FileWriter("research_papers_indexed/query_wise_papers_top_5/title_abstract/"+query_text.replace(" ","_")));
       // BufferedWriter bw1 = new BufferedWriter(new FileWriter("research_papers_indexed/query_wise_test_set/"+query_text.replace(" ","_")+"_scores"));

        org.lemurproject.galago.utility.FixedSizeMinHeap<ScoredDocument> queue1 = new org.lemurproject.galago.utility.FixedSizeMinHeap<>(ScoredDocument.class, 5000, new ScoredDocument.ScoredDocumentComparator());

        String line;
        ArrayList<Long> documents = new ArrayList<Long>();
        while((line=br.readLine())!=null)
        {
            documents.add(Long.parseLong(line));
        }



        org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
        org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(queryParams);

       // bw.write("id\tapplication\talgorithm\timplementation\treview\n");

        for(Long d:documents)
        {

            String docname = retrieval.getDocumentName(d);
            org.lemurproject.galago.core.parse.Document d1 = s.getDocument(docname, p1);
            //String dtext = d1.contentForXMLTag("title")+" "+d1.contentForXMLTag("paperAbstract");
            bw.write(d1.contentForXMLTag("title")+"\t"+d1.contentForXMLTag("paperAbstract").replace("\t"," ").replace("\n"," ")+"\n");
            //bw1.write(d.document+"\t"+d.score+"\n");
        }

        bw.close();
        br.close();
        //bw1.close();
        //System.out.print("size of the results = "+cc);
           return scoredlist.toArray(new ScoredDocument[0]);
    }



}
