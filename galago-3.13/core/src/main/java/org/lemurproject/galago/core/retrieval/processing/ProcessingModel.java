// BSD License (http://lemurproject.org/galago-license)
package org.lemurproject.galago.core.retrieval.processing;

import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.lang.reflect.Constructor;

import java.util.*;

/**
 * An interface that defines the contract for processing a query. There's one
 * method : execute, which takes a fully annotated query tree, and somehow
 * produces a result list.
 *
 *
 * @author irmarc
 */
public abstract class ProcessingModel {

  public abstract ScoredDocument[] execute(Node queryTree, Parameters queryParams) throws Exception;


  public static <T extends ScoredDocument> T[] toReversedArray(FixedSizeMinHeap<T> queue) {
    if (queue.size() == 0) {
      return null;
    }

    T[] items = queue.getSortedArray();
    int r = 1;
    for (T i : items) {
      i.rank = r;
      r++;
    }

    return items;
  }

  /** @deprecated use create instead! */
  @Deprecated
  public static ProcessingModel instance(LocalRetrieval r, Node root, Parameters p) throws Exception {
    return create(r, root, p);
  }
  public static ProcessingModel create(LocalRetrieval r, Node root, org.lemurproject.galago.utility.Parameters p) throws Exception {
    // If we can be being specific about the processing model:

    if (p.containsKey("processingModel")) {
      String modelName = p.getString("processingModel");
      // these are short hand methods of getting some desired proc models:
      switch (modelName) {
        case "rankeddocument": return new org.lemurproject.galago.core.retrieval.processing.RankedDocumentModel(r);
        case "rankeddocumentchanged": return new org.lemurproject.galago.core.retrieval.processing.RankedDocumentModelChanged(r);
        case "rankedpassage": return new org.lemurproject.galago.core.retrieval.processing.RankedPassageModel(r);
        case "maxscore": return new org.lemurproject.galago.core.retrieval.processing.MaxScoreDocumentModel(r);
        case "get-documents-list":
        {
          String documents_indexed="";
          if(p.containsKey("documents-indexed"))
          {
            documents_indexed = p.getString("documents-indexed");
          }
          return new org.lemurproject.galago.core.retrieval.processing.GetDocumentList(r,documents_indexed);
        }
        case "get-documents-list-changed":
        {
          return new org.lemurproject.galago.core.retrieval.processing.GetDocumentListChanged(r);
        }
        case "get-documents-list-evaluation":
        {
          return new org.lemurproject.galago.core.retrieval.processing.GetDocumentListEvaluation(r);
        }
        case "subquery-scorer":
        {
          if((p.containsKey("documents")))
          {
            String file_name = p.getString("documents");
            return new org.lemurproject.galago.core.retrieval.processing.SubqueryScorer(r, file_name);
          }
        }
        case "quad":
        {
          if((p.containsKey("subquery-scores")))
          {
            String file_name = p.getString("subquery-scores");
            return new org.lemurproject.galago.core.retrieval.processing.Quad(r, 0.5, file_name );
          }
        }
        case "aspectmodel":
          {
            String file_name="";
            String index_file="";
            String aspect_prob="";
            String aspect_name = "";
            if(p.containsKey("entitylist"))
            {
              file_name=p.getString("entitylist");
            }
            else
            {
              throw new Exception("entity list not provided");
              //System.exit(0);
            }
            if(p.containsKey("entityindexfile"))
            {
              index_file = p.getString("entityindexfile");
            }
            else
            {
              throw new Exception("entityindexfile not provided");
            }
            if(p.containsKey("aspectprobfile"))
            {
              aspect_prob = p.getString("aspectprobfile");
            }
            else
            {
              throw new Exception("aspect probability file not provided");
            }
            if(p.containsKey("aspectname"))
            {
              aspect_name = p.getString("aspectname");
            }
            else
            {
              throw new Exception("aspect name not provided");
            }
          return new org.lemurproject.galago.core.retrieval.processing.AspectGenerative(r,aspect_prob, index_file, aspect_name, file_name);
        }
        case "aspectmodel2":
        {
          String file_name="";
          String index_file="";
          String aspect_prob="";
          String aspect_name = "";
          if(p.containsKey("entitylist"))
          {
            file_name=p.getString("entitylist");
          }
          else
          {
            throw new Exception("entity list not provided");
            //System.exit(0);
          }
          if(p.containsKey("entityindexfile"))
          {
            index_file = p.getString("entityindexfile");
          }
          else
          {
            throw new Exception("entityindexfile not provided");
          }
          if(p.containsKey("aspectprobfile"))
          {
            aspect_prob = p.getString("aspectprobfile");
          }
          else
          {
            throw new Exception("aspect probability file not provided");
          }
          if(p.containsKey("aspectname"))
          {
            aspect_name = p.getString("aspectname");
          }
          else
          {
            throw new Exception("aspect name not provided");
          }
          return new org.lemurproject.galago.core.retrieval.processing.AspectTranslational(r,aspect_prob, index_file, aspect_name, file_name);
        }
        case "aspectmixturemodel":
        {
          String entitylist="";
          String entity_index_file="";
          String word_index_file="";
          String aspect_name = "";
          int feedback=1000;
          String mixturefile="";
          String scorer="";
          String term_statistics="";
          boolean infer=false;
          boolean contains_entity=true;



          if(p.containsKey("wordindexfile"))
          {
            word_index_file = p.getString("wordindexfile");
          }
          else
          {
            throw new Exception("wordindexfile not provided");
          }

          if(p.containsKey("aspectname"))
          {
            aspect_name = p.getString("aspectname");
          }
          else
          {
            throw new Exception("aspect name not provided");
          }

          if(p.containsKey("mixturefile"))
          {
            mixturefile = p.getString("mixturefile");
          }
          else
          {
            throw new Exception("mixture file not provided");
          }
          if(p.containsKey("term-statistics"))
          {
            term_statistics = p.getString("term-statistics");
          }
          else
          {
            throw new Exception("term statistics file not provided");
          }

          double lambda_1=0.5;
          double lambda_2=0.5;

          if(p.containsKey("lambda1"))
          {
            lambda_1 = p.getDouble("lambda1");
          }

          if(p.containsKey("lambda2"))
          {
            lambda_2 = p.getDouble("lambda2");
          }




          /*else
          {
            throw new Exception("lambda2 parameter not provided");
          }*/
          if(p.containsKey("scorer"))
          {
            scorer = p.getString("scorer");
          }
          else
          {
            throw new Exception("scorer parameter not provided");
          }
          if(p.containsKey("feedback"))
          {
            feedback = p.getInt("feedback");
          }
          String documents="";
          if(p.containsKey("documents"))
          {
            documents = p.getString("documents");
          }

          return new org.lemurproject.galago.core.retrieval.processing.AspectMixtureModel(r, word_index_file, aspect_name,mixturefile, scorer,term_statistics, feedback, documents);
          //return new org.lemurproject.galago.core.retrieval.processing.AspectMixtureModel(r);

        }
        case "aspectmixturemodeladded":
        {
          String entitylist="";
          String entity_index_file="";
          String aspect_prob="";
          String aspect_name = "";
          String wordprob = "";
          String mixturefile="";
          if(p.containsKey("entitylist"))
          {
            entitylist=p.getString("entitylist");
          }
          else
          {
            throw new Exception("entity list not provided");
            //System.exit(0);
          }
          if(p.containsKey("entityindexfile"))
          {
            entity_index_file = p.getString("entityindexfile");
          }
          else
          {
            throw new Exception("entityindexfile not provided");
          }
          if(p.containsKey("aspectprobfile"))
          {
            aspect_prob = p.getString("aspectprobfile");
          }
          else
          {
            throw new Exception("aspect probability file not provided");
          }
          if(p.containsKey("aspectname"))
          {
            aspect_name = p.getString("aspectname");
          }
          else
          {
            throw new Exception("aspect name not provided");
          }
          if(p.containsKey("wordprob"))
          {
            wordprob = p.getString("wordprob");
          }
          else
          {
            throw new Exception("word prob file not provided");
          }
          if(p.containsKey("mixturefile"))
          {
            mixturefile = p.getString("mixturefile");
          }
          else
          {
            throw new Exception("mixture file not provided");
          }
          String smooth="";
          String abstracts="";
          String mixturechoice="";
          double lambda_1=0.5;
          double lambda_2=0.5;
          if(p.containsKey("smooth"))
          {
            smooth = p.getString("smooth");
          }
          else
          {
            throw new Exception("smooth parameter not provided");
          }
          if(p.containsKey("abstracts"))
          {
            abstracts = p.getString("abstracts");
          }
          else
          {
            throw new Exception("abstracts parameter not provided");
          }
          if(p.containsKey("lambda1"))
          {
            lambda_1 = p.getDouble("lambda1");
          }
        /*  else
          {
            throw new Exception("lambda1 parameter not provided");
          }*/
          if(p.containsKey("lambda2"))
          {
            lambda_2 = p.getDouble("lambda2");
          }
           /* else
          {
            throw new Exception("lambda2 parameter not provided");
          }*/
          if(p.containsKey("mixturechoice"))
          {
            mixturechoice = p.getString("mixturechoice");

          }
          else
          {
            throw new Exception("mixturechoice parameter not provided");
          }

          return new org.lemurproject.galago.core.retrieval.processing.AspectMixtureModelAdded(r,entitylist, entity_index_file, wordprob, aspect_prob, aspect_name, lambda_1, lambda_2, mixturefile, smooth, abstracts, mixturechoice);
        }
        case "mixturemodelentities":
        {
          String entitylist="";
          String entity_index_file="";
          String aspect_prob="";
          String aspect_name = "";
          String wordprob = "";
          String mixturefile="";
          if(p.containsKey("entitylist"))
          {
            entitylist=p.getString("entitylist");
          }
          else
          {
            throw new Exception("entity list not provided");
            //System.exit(0);
          }
          if(p.containsKey("entityindexfile"))
          {
            entity_index_file = p.getString("entityindexfile");
          }
          else
          {
            throw new Exception("entityindexfile not provided");
          }
          if(p.containsKey("aspectprobfile"))
          {
            aspect_prob = p.getString("aspectprobfile");
          }
          else
          {
            throw new Exception("aspect probability file not provided");
          }
          if(p.containsKey("aspectname"))
          {
            aspect_name = p.getString("aspectname");
          }
          else
          {
            throw new Exception("aspect name not provided");
          }
          if(p.containsKey("wordprob"))
          {
            wordprob = p.getString("wordprob");
          }
          else
          {
            throw new Exception("word prob file not provided");
          }
          if(p.containsKey("mixturefile"))
          {
            mixturefile = p.getString("mixturefile");
          }
          else
          {
            throw new Exception("mixture file not provided");
          }
          String smooth="";
          String abstracts="";
          double lambda_1=0.5;
          double lambda_2=0.5;
          if(p.containsKey("smooth"))
          {
            smooth = p.getString("smooth");
          }
          else
          {
            throw new Exception("smooth parameter not provided");
          }
          if(p.containsKey("abstracts"))
          {
            abstracts = p.getString("abstracts");
          }
          else
          {
            throw new Exception("abstracts parameter not provided");
          }
          if(p.containsKey("lambda1"))
          {
            lambda_1 = p.getDouble("lambda1");
          }
         /* else
          {
            throw new Exception("lambda1 parameter not provided");
          }*/
          if(p.containsKey("lambda2"))
          {
            lambda_2 = p.getDouble("lambda1");
          }
          /*else
          {
            throw new Exception("lambda2 parameter not provided");
          }*/
          return new org.lemurproject.galago.core.retrieval.processing.AspectMixtureModelEntities(r,entitylist, entity_index_file, wordprob, aspect_prob, aspect_name, lambda_1, lambda_2, mixturefile, smooth, abstracts);
        }
       /* case "rankeddocumentchanged":
        {
          String entitylist="";
          String entity_index_file="";
          String aspect_prob="";
          String aspect_name = "";
          String wordprob = "";
          String mixturefile="";
          if(p.containsKey("entitylist"))
          {
            entitylist=p.getString("entitylist");
          }
          else
          {
            throw new Exception("entity list not provided");
            //System.exit(0);
          }
          if(p.containsKey("entityindexfile"))
          {
            entity_index_file = p.getString("entityindexfile");
          }
          else
          {
            throw new Exception("entityindexfile not provided");
          }
          if(p.containsKey("aspectprobfile"))
          {
            aspect_prob = p.getString("aspectprobfile");
          }
          else
          {
            throw new Exception("aspect probability file not provided");
          }
          if(p.containsKey("aspectname"))
          {
            aspect_name = p.getString("aspectname");
          }
          else
          {
            throw new Exception("aspect name not provided");
          }
          if(p.containsKey("wordprob"))
          {
            wordprob = p.getString("wordprob");
          }
          else
          {
            throw new Exception("word prob file not provided");
          }
          if(p.containsKey("mixturefile"))
          {
            mixturefile = p.getString("mixturefile");
          }
          else
          {
            throw new Exception("mixture file not provided");
          }
          return new org.lemurproject.galago.core.retrieval.processing.RankedDocumentModelChanged(r,entitylist, entity_index_file, wordprob, aspect_prob, aspect_name, 0.5, 0.5, mixturefile);
        }*/

        case "setmodel": return new org.lemurproject.galago.core.retrieval.processing.SetModel(r);
        case "entitiesindexing":
          {
            String file_name="";
            String index_file="";
            String documents_indexed_file="";
           // HashSet<Long> h = new HashSet<Long>();
            if(p.containsKey("entitylist"))
            {
              file_name=p.getString("entitylist");
            }
            else
            {

              throw new Exception("entity list not provided");
              //System.exit(0);
            }
            if(p.containsKey("entityindexfile"))
            {
              index_file = p.getString("entityindexfile");
            }
            else
            {
              throw new Exception("entityindexfile not provided");
            }
            if(p.containsKey("documentsindexedfile"))
            {
              documents_indexed_file = p.getString("documentsindexedfile");
            }
            else
            {
              throw new Exception("documents indexed file not provided");
            }
          return new org.lemurproject.galago.core.retrieval.processing.EntitiesIndexingModel(r,file_name, index_file, documents_indexed_file);
        }
        case "getalldocs":
        {
          return new org.lemurproject.galago.core.retrieval.processing.ReturnAllDocuments(r);
        }
        // CURRENTLY BROKEN DO NOT USE
//      } else if (modelName.equals("wand")) {
//        return new WANDScoreDocumentModel(r);
        default:
          // generally it's better to use the full class
          Class<?> clazz = Class.forName(modelName);
          Constructor<?> cons = clazz.getConstructor(LocalRetrieval.class);
          return (ProcessingModel) cons.newInstance(r);
      }
    }

    // if there's a working set:
    if (p.containsKey("working")) {
      if (p.get("extentQuery", false)) {
        return new org.lemurproject.galago.core.retrieval.processing.WorkingSetExtentModel(r);
      } else if (p.get("passageQuery", false)) {
        return new org.lemurproject.galago.core.retrieval.processing.WorkingSetPassageModel(r);
      } else {
        return new org.lemurproject.galago.core.retrieval.processing.WorkingSetDocumentModel(r);
      }
    }

    if (p.get("passageQuery", false)) {
      return new org.lemurproject.galago.core.retrieval.processing.RankedPassageModel(r);
    } else {
      if (p.get("deltaReady", false)) {
        return new org.lemurproject.galago.core.retrieval.processing.MaxScoreDocumentModel(r);
      } else {
        return new org.lemurproject.galago.core.retrieval.processing.RankedDocumentModel(r);
      }
    }
  }
}
