package org.lemurproject.galago.core.retrieval.processing;


import org.lemurproject.galago.core.retrieval.processing.ProcessingModel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.CountIterator;
import org.lemurproject.galago.core.retrieval.iterator.ScoreIterator;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import java.io.*;
import java.util.*;

import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Emit;

public class Quad extends ProcessingModel
{
    org.lemurproject.galago.core.retrieval.LocalRetrieval retrieval;
    org.lemurproject.galago.core.index.Index index;
    public int feedback;
    public double lambda;
    public String aspect_scores;
    public HashMap<Long, ArrayList<Double>> probs;

    public Quad(org.lemurproject.galago.core.retrieval.LocalRetrieval lr, double lambda, String aspect_scores) throws Exception
    {
        this.retrieval=lr;
        this.index = retrieval.getIndex();
        this.lambda=lambda;
        this.feedback=1000;

        HashMap<Long, ArrayList<Double>> probs_all = new HashMap<Long, ArrayList<Double>>();
        BufferedReader br = new BufferedReader(new FileReader(aspect_scores));
        String line;
        while((line=br.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            ArrayList<Double> probs1 = new ArrayList<Double>();
            while(tok.hasMoreTokens())
            {
                String a = tok.nextToken();
                tokens.add(a);
                //double score = Double.parseDouble(a);
                //probs1.add(score);
            }
            int count=0;
            Long doc=0L;
            for(String t:tokens)
            {
                if(count>0)
                {
                    if(t.equals("null"))
                    {
                        probs1.add(0.0);
                    }
                    else
                        {
                        double score = Double.parseDouble(t);
                        probs1.add(Math.exp(score));
                    }
                }
                else
                {
                    doc=Long.parseLong(t);
                    count++;
                }
            }
            probs_all.put(doc,probs1);
        }
        this.probs = probs_all;
    }

    public org.lemurproject.galago.core.retrieval.ScoredDocument[] execute(org.lemurproject.galago.core.retrieval.query.Node queryTree, org.lemurproject.galago.utility.Parameters queryParams) throws Exception
    {
        System.out.println("in execute");
        System.out.println("Completed reading auxillary data structures: " + System.nanoTime());
        int requested = queryParams.get("requested", 1000);
        boolean annotate = queryParams.get("annotate", false);

        org.lemurproject.galago.core.retrieval.processing.ScoringContext context = new org.lemurproject.galago.core.retrieval.processing.ScoringContext();
        String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();
        System.out.println(query_text);
        org.lemurproject.galago.utility.FixedSizeMinHeap<org.lemurproject.galago.core.retrieval.ScoredDocument> queue1 = new org.lemurproject.galago.utility.FixedSizeMinHeap<>(org.lemurproject.galago.core.retrieval.ScoredDocument.class, requested, new org.lemurproject.galago.core.retrieval.ScoredDocument.ScoredDocumentComparator());

        ScoreIterator iterator = (ScoreIterator) retrieval.createIterator(queryParams, queryTree);
        ScoreIterator iterator1 = (ScoreIterator) retrieval.createIterator(queryParams, queryTree);

        HashMap<Long, Double> rel = new HashMap<Long, Double>();
        while(!iterator1.isDone())
        {
            long document = iterator1.currentCandidate();
            context.document = document;
            iterator1.syncTo(document);
            if (iterator1.hasMatch(context))
            {
                double score = iterator1.score(context);

                if (queue1.size() < feedback || queue1.peek().score < score)
                {
                    rel.put(document,score);
                    ScoredDocument scoredDocument = new ScoredDocument(document, score);
                    if (annotate)
                    {
                        scoredDocument.annotation = iterator1.getAnnotatedNode(context);
                    }
                    queue1.offer(scoredDocument);
                }
            }
            iterator1.movePast(document);
        }
        HashSet<Long> documents = new HashSet<Long>();
        int count=feedback;
        for(ScoredDocument d:toReversedArray(queue1))
        {
            documents.add(d.document);
        }

        System.out.println("documents size: "+documents.size());

        List<ScoredDocument> results;

        HashSet<Long> new_documents = new HashSet<Long>();

        org.lemurproject.galago.utility.FixedSizeMinHeap<org.lemurproject.galago.core.retrieval.ScoredDocument> queue = new org.lemurproject.galago.utility.FixedSizeMinHeap<>(org.lemurproject.galago.core.retrieval.ScoredDocument.class, requested, new org.lemurproject.galago.core.retrieval.ScoredDocument.ScoredDocumentComparator());


        for(Long d:documents)
        {
            double coverage=0.0;
            double novelty=1.0;
            double diversity=0.0;
            double score=0.0;
            int index=0;

            if(probs.get(d)!=null) {
                //System.out.println(d);
                // continue;



                for(double d1:probs.get(d))
                {
                    for(Long nd:new_documents)
                    {
                        novelty=novelty*(1-probs.get(nd).get(index));
                    }
                    index++;
                    coverage = d1;
                    diversity = diversity + coverage*novelty;
                }
            }

            score = (1-lambda)*Math.exp(rel.get(d)) + lambda*diversity;

            if (queue.size() < 5 || queue.peek().score < score)
            {
                ScoredDocument scoredDocument = new ScoredDocument(d, score);
                queue.offer(scoredDocument);
            }

        }

        return toReversedArray(queue);

    }
}