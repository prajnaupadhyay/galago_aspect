// BSD License (http://lemurproject.org/galago-license)
package org.lemurproject.galago.core.retrieval.processing;

import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.ScoreIterator;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

/**
 * Performs straightforward document-at-a-time (daat) processing of a fully
 * annotated query, processing scores over documents.
 *
 * @author irmarc, sjh
 */
public class RankedDocumentModelChanged extends ProcessingModel {

  LocalRetrieval retrieval;
  Index index;

  public RankedDocumentModelChanged(LocalRetrieval lr) {
    retrieval = lr;
    this.index = retrieval.getIndex();
  }

  @Override
  public ScoredDocument[] execute(Node queryTree, Parameters queryParams) throws Exception {
    // This model uses the simplest ScoringContext
    ScoringContext context = new ScoringContext();
    String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();;
    System.out.println(query_text);
    // Number of documents requested.
    int requested = queryParams.get("requested", 1000);
    boolean annotate = queryParams.get("annotate", false);

    // Maintain a queue of candidates
    FixedSizeMinHeap<ScoredDocument> queue = new FixedSizeMinHeap<>(ScoredDocument.class, requested, new ScoredDocument.ScoredDocumentComparator());
    org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
    org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(queryParams);

    // construct the iterators -- we use tree processing
    ScoreIterator iterator = (ScoreIterator) retrieval.createIterator(queryParams, queryTree);
    //org.lemurproject.galago.core.retrieval.iterator.scoring.DirichletScoringIterator iterator = (org.lemurproject.galago.core.retrieval.iterator.scoring.DirichletScoringIterator) retrieval.createIterator(queryParams, queryTree);

    // now there should be an iterator at the root of this tree
    int count=0;
    while (!iterator.isDone()) {
      long document = iterator.currentCandidate();
       //String docname = retrieval.getDocumentName(document);
      // org.lemurproject.galago.core.parse.Document d = s.getDocument(docname, p1);

      //System.out.println(d.metadata);
      // This context is shared among all scorers
      context.document = document;
      iterator.syncTo(document);

      if (iterator.hasMatch(context))
      {
        count++;
        double score = iterator.score(context);

        //System.out.println("queue.peek().score= "+queue.peek().score);


        if (queue.size() < requested || queue.peek().score < score)
        {
          ScoredDocument scoredDocument = new ScoredDocument(document, score);
          if (annotate)
          {
            scoredDocument.annotation = iterator.getAnnotatedNode(context);
          }
          queue.offer(scoredDocument);
        }
      }
      iterator.movePast(document);
    }
    System.out.println("total documents: "+count);
    return toReversedArray(queue);
  }
}
