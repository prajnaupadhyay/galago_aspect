package org.lemurproject.galago.core.retrieval.processing;

import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.ScoreIterator;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.util.*;

/**
 * it returns all the documents instead of pruning the size to 'requested' number of results.
 */
public class ReturnAllDocuments extends org.lemurproject.galago.core.retrieval.processing.ProcessingModel {

    LocalRetrieval retrieval;
    Index index;

    public ReturnAllDocuments(LocalRetrieval lr) {
        retrieval = lr;
        this.index = retrieval.getIndex();
    }

    @Override
    public ScoredDocument[] execute(Node queryTree, Parameters queryParams) throws Exception {
        // This model uses the simplest ScoringContext
        org.lemurproject.galago.core.retrieval.processing.ScoringContext context = new org.lemurproject.galago.core.retrieval.processing.ScoringContext();
        String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();;
        System.out.println(query_text);
        // Number of documents requested.
        int requested = queryParams.get("requested", 1000);
        boolean annotate = queryParams.get("annotate", false);

        // Maintain a queue of candidates
       ArrayList<ScoredDocument> queue = new ArrayList<ScoredDocument>();

        // construct the iterators -- we use tree processing
        ScoreIterator iterator = (ScoreIterator) retrieval.createIterator(queryParams, queryTree);

        // now there should be an iterator at the root of this tree
        while (!iterator.isDone()) {
            long document = iterator.currentCandidate();

            // This context is shared among all scorers
            context.document = document;
            iterator.syncTo(document);
            if (iterator.hasMatch(context))
            {
                double score = iterator.score(context);
                ScoredDocument scoredDocument = new ScoredDocument(document, score);
                queue.add(scoredDocument);
               /* if (queue.peek().score < score) {
                    ScoredDocument scoredDocument = new ScoredDocument(document, score);
                    if (annotate) {
                        scoredDocument.annotation = iterator.getAnnotatedNode(context);
                    }
                    queue.offer(scoredDocument);
                }*/
            }
            iterator.movePast(document);
        }

        return queue.toArray(new ScoredDocument[1]);
    }
}
