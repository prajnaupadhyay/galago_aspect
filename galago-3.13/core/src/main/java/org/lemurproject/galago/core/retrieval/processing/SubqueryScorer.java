package org.lemurproject.galago.core.retrieval.processing;


import org.lemurproject.galago.core.index.Index;
import org.lemurproject.galago.core.retrieval.LocalRetrieval;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.iterator.ScoreIterator;
import org.lemurproject.galago.core.retrieval.processing.ProcessingModel;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.utility.FixedSizeMinHeap;
import org.lemurproject.galago.utility.Parameters;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.StringTokenizer;

public class SubqueryScorer extends ProcessingModel
{
    org.lemurproject.galago.core.retrieval.LocalRetrieval retrieval;
    org.lemurproject.galago.core.index.Index index;
    String doc_file;
    HashSet<Long> documents;


    public SubqueryScorer(org.lemurproject.galago.core.retrieval.LocalRetrieval lr, String doc_file) throws Exception
    {
        this.retrieval=lr;
        this.index = retrieval.getIndex();
        BufferedReader br = new BufferedReader(new FileReader(doc_file));
        String line;
        HashSet<Long> documents = new HashSet<Long>();
        while((line=br.readLine())!=null)
        {
            documents.add(Long.parseLong(line));

        }
        this.documents = documents;
    }

    public org.lemurproject.galago.core.retrieval.ScoredDocument[] execute(org.lemurproject.galago.core.retrieval.query.Node queryTree, Parameters queryParams) throws Exception
    {
        System.out.println("in execute");
        System.out.println("Completed reading auxillary data structures: " + System.nanoTime());
        int requested = queryParams.get("requested", 1000);
        boolean annotate = queryParams.get("annotate", false);

        org.lemurproject.galago.core.retrieval.processing.ScoringContext context = new org.lemurproject.galago.core.retrieval.processing.ScoringContext();
        String query_text = queryTree.getText().replaceAll("\\s{2,}", " ").trim();
        System.out.println(query_text);
        FixedSizeMinHeap<org.lemurproject.galago.core.retrieval.ScoredDocument> queue1 = new FixedSizeMinHeap<>(org.lemurproject.galago.core.retrieval.ScoredDocument.class, requested, new org.lemurproject.galago.core.retrieval.ScoredDocument.ScoredDocumentComparator());

        ScoreIterator iterator = (ScoreIterator) retrieval.createIterator(queryParams, queryTree);
        ScoreIterator iterator1 = (ScoreIterator) retrieval.createIterator(queryParams, queryTree);

        HashMap<Long, Double> rel = new HashMap<Long, Double>();
        while(!iterator1.isDone())
        {
            long document = iterator1.currentCandidate();
            context.document = document;
            iterator1.syncTo(document);
            if (iterator1.hasMatch(context))
            {
                double score = iterator1.score(context);

                if (documents.contains(document))
                {
                    rel.put(document,score);
                    ScoredDocument scoredDocument = new ScoredDocument(document, score);
                    if (annotate)
                    {
                        scoredDocument.annotation = iterator1.getAnnotatedNode(context);
                    }
                    queue1.offer(scoredDocument);
                }
            }
            iterator1.movePast(document);
        }


        return toReversedArray(queue1);

    }
}