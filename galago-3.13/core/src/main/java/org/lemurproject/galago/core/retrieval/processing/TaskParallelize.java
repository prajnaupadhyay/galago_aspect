package org.lemurproject.galago.core.retrieval.processing;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.*;

/**
 * A task that does all the randomwalks for each node.
 * @author pearl
 *
 */
public class TaskParallelize implements Runnable
{
    HashMap<Integer, Double> mixture;
    HashMap<Integer,Double> document_prob;
    ConcurrentHashMap<Long, Double> scores;
    HashMap<Integer, Double> background;
    long document;
    String scorer;
    double min;
    double mu;

    public TaskParallelize(HashMap<Integer, Double> mixture, HashMap<Integer, Double> document_prob, ConcurrentHashMap<Long, Double> scores, HashMap<Integer, Double> background, long document, String scorer, double min, double mu)
    {
        this.mixture=mixture;
        this.document_prob=document_prob;
        this.scores=scores;
        this.background=background;
        this.document=document;
        this.scorer = scorer;
        this.min=min;
        this.mu=mu;
    }
    public void run()
    {
        double back_prob=0.0;
        for(int s:background.keySet())
        {
            back_prob = back_prob + background.get(s);
        }
        for(int s:background.keySet())
        {
            background.put(s,background.get(s)/back_prob);
        }
        if(scorer.equals("kl-divergence2"))
        {
            double number=0.0;
            double count1=0.0;
            double tot_score=0.0;

            for(int h:document_prob.keySet())
            {
                number = number + document_prob.get(h);
                if (mixture.get(h) == null)
                {
                    count1++;
                }

            }

            if(count1>0)
            {
                for(int t:mixture.keySet())
                {
                    mixture.put(t, mixture.get(t) - (min/100)) ;
                }
                for(int h:document_prob.keySet())
                {

                    if (mixture.get(h) == null)
                    {
                        mixture.put(h, ((mixture.size() * min) / (100 * count1)));
                    }

                }
            }




            double score1=0.0;
            double tot_prob1=0.0;
            double tot_prob2=0.0;

            for (int t : mixture.keySet())
            {

                double smooth = 0.0;
                if (document_prob.get(t) == null)
                    smooth = mu*background.get(t)/(number+mu);
                else
                {
                    smooth = (document_prob.get(t)+(mu*background.get(t)))/(number+mu);
                    //System.out.println("hh.get(t): "+hh.get(t)+", number: "+number);
                }

                tot_prob1=tot_prob1+smooth;
                tot_prob2=tot_prob2+mixture.get(t);

                score1 = score1 + mixture.get(t) * (Math.log(mixture.get(t)) - Math.log(smooth));



            }

            tot_score = 1/(score1);
            scores.put(document,tot_score);
            //System.out.println(document+"\t"+tot_prob1);
           // System.out.println("mixture\t"+tot_prob2);


        }

    }

}
