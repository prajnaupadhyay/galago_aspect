// BSD License (http://lemurproject.org/galago-license)
package org.lemurproject.galago.core.tools.apps;

import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.processing.AspectMixtureModel;
import org.lemurproject.galago.core.retrieval.processing.AspectMixtureModelEntities;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.queries.JSONQueryFormat;
import org.lemurproject.galago.utility.tools.AppFunction;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.tools.Arguments;
import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Emit;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.*;

/**
 *
 * @author sjh
 */
public class BatchSearch extends AppFunction {

  public static final Logger logger = Logger.getLogger("BatchSearch");

  public static void main(String[] args) throws Exception
  {
    (new BatchSearch()).run(Arguments.parse(args), System.out);
  }

  @Override
  public String getName() {
    return "batch-search";
  }

  @Override
  public String getHelpString() {
    return "galago simple-batch-search <args>\n\n"
            + "  Runs a batch of queries against an index and produces TREC-formatted\n"
            + "  output.  The output can be used with retrieval evaluation tools like\n"
            + "  galago eval (org.lemurproject.galago.core.eval).\n\n"
            + "  Sample invocation:\n"
            + "     galago batch-search --index=/tmp/myindex --requested=200 /tmp/queries.json \n\n"
            + "  Args:\n"
            + "     --index=path_to_your_index\n"
            + "     --requested : Number of results to return for each query, default=1000\n"
            + "     --operatorWrap=operator  : Wrap query text in the specified operator.\n"
            + "     --queryFormat=json|tsv   : Accept query file in JSON or TSV format.  default=json\n"
            + "     /path/to/query/file.json : Input file in xml parameters format (see below).\n\n"
            + "  Query file format:\n"
            + "    The query file is an JSON file containing a set of queries.  Each query\n"
            + "    has text field, which contains the text of the query, and a number field, \n"
            + "    which uniquely identifies the query in the output.\n\n"
            + "  Example query file:\n"
            + "  {\n"
            + "     \"queries\" : [\n"
            + "       {\n"
            + "         \"number\" : \"CACM-408\", \n"
            + "         \"text\" : \"#combine(my query)\"\n"
            + "       },\n"
            + "       {\n"
            + "         \"number\" : \"WIKI-410\", \n"
            + "         \"text\" : \"#combine(another query)\" \n"
            + "       }\n"
            + "    ]\n"
            + "  }\n";
  }

  @Override
  public void run(Parameters parameters, PrintStream out) throws Exception {
    List<ScoredDocument> results;

    if (!(parameters.containsKey("query") || parameters.containsKey("queries")))
    {
      out.println(this.getHelpString());
      return;
    }
    if(parameters.containsKey("entitylist"))
    {
      System.out.println("yeah");
    }

    // ensure we can print to a file instead of the commandline
    /*if (parameters.isString("outputFile"))
    {
      boolean append = parameters.get("appendFile", true);
      out = new PrintStream(new BufferedOutputStream(
              new FileOutputStream(parameters.getString("outputFile"), append)), true, "UTF-8");
    }*/

    if (parameters.isString("outputFile"))
    {
      boolean append = parameters.get("appendFile", true);
      out = new PrintStream(new BufferedOutputStream(
              new FileOutputStream(parameters.getString("outputFile"), append)), true, "UTF-8");
    }

    // get queries
    List<Parameters> queries;
    String queryFormat = parameters.get("queryFormat", "json").toLowerCase();
    switch (queryFormat)
    {
      case "json":
        queries = JSONQueryFormat.collectQueries(parameters);
        break;
      case "tsv":
        queries = JSONQueryFormat.collectTSVQueries(parameters);
        break;
      default: throw new IllegalArgumentException("Unknown queryFormat: "+queryFormat+" try one of JSON, TSV");
    }

    // open index
    Retrieval retrieval = RetrievalFactory.create(parameters);
    System.out.println("here");

    // record results requested
    int requested = (int) parameters.get("requested", 1000);

    // for each query, run it, get the results, print in TREC format
    HashSet<Integer> h = new HashSet<Integer>();


    String entitylist="";
    String entity_index_file="";
    String aspect_prob="";
    String aspect_name = "";
    String wordprob = "";
    String mixturefile="";
    if(parameters.containsKey("entitylist"))
    {
      entitylist=parameters.getString("entitylist");
    }

    if(parameters.containsKey("entityindexfile"))
    {
      entity_index_file = parameters.getString("entityindexfile");
    }

    if(parameters.containsKey("aspectprobfile"))
    {
      aspect_prob = parameters.getString("aspectprobfile");
    }

    if(parameters.containsKey("aspectname"))
    {
      aspect_name = parameters.getString("aspectname");
    }

    if(parameters.containsKey("wordprob"))
    {
      wordprob = parameters.getString("wordprob");
    }

    if(parameters.containsKey("mixturefile"))
    {
      mixturefile = parameters.getString("mixturefile");
    }

    String smooth="";
    String abstracts="";
    double lambda_1=0.5;
    double lambda_2=0.5;
    if(parameters.containsKey("smooth"))
    {
      smooth = parameters.getString("smooth");
    }

    if(parameters.containsKey("abstracts"))
    {
      abstracts = parameters.getString("abstracts");
    }

    if(parameters.containsKey("lambda1"))
    {
      lambda_1 = parameters.getDouble("lambda1");
    }

    if(parameters.containsKey("lambda2"))
    {
      lambda_2 = parameters.getDouble("lambda2");
    }
    AspectMixtureModelEntities aa=null;
    if(!(parameters.getString("processingModel").equals("rankeddocument") || parameters.getString("processingModel").equals("rankedpassage") || parameters.getString("processingModel").equals("maxscore") || parameters.getString("processingModel").equals("aspectmixturemodel") || parameters.getString("processingModel").equals("rankeddocumentchanged")))
    {
      System.out.println("entered here");
       aa = new AspectMixtureModelEntities(entitylist, entity_index_file, wordprob, aspect_prob, aspect_name, lambda_1, lambda_2, mixturefile, smooth, abstracts);
    }
    long stime = System.nanoTime();
  //  System.out.println("Start time: "+stime);
    for (Parameters query : queries)
    {

      String queryText = query.getString("text");
      System.out.println("text of the query is: "+queryText);
      String queryNumber = query.getString("number");


      query.setBackoff(parameters);
      query.set("requested", requested);

      // option to fold query cases -- note that some parameters may require upper case
      if (query.get("casefold", false))
      {
        queryText = queryText.toLowerCase();
      }

      if (parameters.get("verbose", false))
      {
        logger.info("RUNNING: " + queryNumber + " : " + queryText);
      }

      // parse and transform query into runnable form
      Node root = StructuredQuery.parse(queryText);

      // --operatorWrap=sdm will now #sdm(...text... here)
      if(parameters.isString("operatorWrap"))
      {
          if(root.getOperator().equals("root"))
          {
            root.setOperator(parameters.getString("operatorWrap"));
          } else {
            Node oldRoot = root;
            root = new Node(parameters.getString("operatorWrap"));
            root.add(oldRoot);
          }
      }
      //String file_name=parameters.getString("entitylist");
      Node transformed = retrieval.transformQuery(root, query);
      String query_text = transformed.getText().replaceAll("\\s{2,}", " ").trim();

     /* BufferedReader br1 = new BufferedReader(new FileReader(file_name));
      String line;
      HashMap<String, Integer> entities = new HashMap<String, Integer>();
      while((line=br1.readLine())!=null)
      {
        StringTokenizer tok = new StringTokenizer(line,"\t");
        ArrayList<String> tokens = new ArrayList<String>();
        while(tok.hasMoreTokens())
        {
          tokens.add(tok.nextToken());
          //entities.add(to)
        }
        entities.put(tokens.get(1).replace("_"," "),Integer.parseInt(tokens.get(0)));
      }
     // String query_text = transformed.getText().replaceAll("\\s{2,}", " ").trim();
      Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
      for(String keyword : entities.keySet())
      {
        trie_builder = trie_builder.addKeyword(keyword);
      }
      Trie trie1 = trie_builder.build();


*/

     Trie trie1=null;

     if(!(parameters.getString("processingModel").equals("rankeddocument") || parameters.getString("processingModel").equals("rankedpassage") || parameters.getString("processingModel").equals("maxscore") || parameters.getString("processingModel").equals("aspectmixturemodel") || parameters.getString("processingModel").equals("rankeddocumentchanged")))
     {
      if (aa.entity_prob.get(query_text) == null)
      {
        continue;
      }


      aa.mix(query_text, mixturefile);

      //Trie trie1;
      Trie.TrieBuilder trie_builder = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();

      for (String keyword : aa.entity_prob.get(query_text).keySet()) {
        trie_builder = trie_builder.addKeyword(keyword);
      }
      trie1 = trie_builder.build();
    }




      if (parameters.get("verbose", false))
      {
        logger.info("Transformed Query:\n" + transformed.toPrettyString());
      }

      // run query
      results = retrieval.executeQuery(transformed, parameters).scoredDocuments;
      System.out.println("\n"+results.size()+"\n");
      org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
      org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(parameters);
      int count=0;
      //System.out.println("id\tapplication\talgorithm\timplementation\treview\n");
      for(ScoredDocument n:results)
      {
        //if(count==10) break;
        //count++;

        //System.out.println(n.score);
        //System.out.println(n.document);
        String docname = retrieval.getDocumentName(n.document);
        org.lemurproject.galago.core.parse.Document d = s.getDocument(docname, p1);
        String dtext = d.contentForXMLTag("title")+"\t"+d.contentForXMLTag("paperAbstract");
        out.print(n+"\t"+dtext+"\t"+n.score+"\n");
        //System.out.println(dtext+"\n ");
       // System.out.println(n.document+"\t1\t1\t1\t"+dtext+"\n");
        /*Collection<Emit> named_entity_occurences = trie1.parseText(dtext.toLowerCase().replace("-"," "));
        Set<String> named_entities = new HashSet<String>(); // Here set is created additionally to handle cases where named entity is present more than once in a document



        for (Emit named_entity_occurence : named_entity_occurences)
        {
          if(!query_text.contains(named_entity_occurence.getKeyword()))
          retained_entities.add(named_entity_occurence.getKeyword());

        }*/
        if(!(parameters.getString("processingModel").equals("rankeddocument") || parameters.getString("processingModel").equals("rankedpassage") || parameters.getString("processingModel").equals("maxscore") || parameters.getString("processingModel").equals("aspectmixturemodel")))
        {

          HashSet<String> retained_entities = new HashSet<String>();

          for (int j : aa.freq_prob.get(n.document).keySet()) {
            if (aa.integer_to_string.get(j) != null) {
              String ename = aa.integer_to_string.get(j);
              if (!query_text.contains(ename) && aa.mixture.get(ename) != null) {
                retained_entities.add(ename);
              }
            }
          }
          for (String s1 : retained_entities)
          {
            System.out.println(s1);
          }
        }

       // System.out.println(d.text);
      }

      // if we have some results -- print in to output stream
     /* if (!results.isEmpty())
      {
        for (ScoredDocument sd : results)
        {
          if (query.get("trec", false))
          {
            out.println(sd.toTRECformat(queryNumber));
          }
          else
          {
            out.println(sd.toString(queryNumber));
          }
        }
      }*/
    }
    long etime = System.nanoTime();
    System.out.println("End time: "+etime);
    System.out.println("Time taken: "+(etime-stime)/1000000000);

    if (parameters.isString("outputFile")) {
      out.close();
    }
  }

}
