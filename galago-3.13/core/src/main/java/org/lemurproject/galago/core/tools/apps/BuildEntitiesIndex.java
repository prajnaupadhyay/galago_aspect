// BSD License (http://lemurproject.org/galago-license)
package org.lemurproject.galago.core.tools.apps;

import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.processing.AspectMixtureModel;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.queries.JSONQueryFormat;
import org.lemurproject.galago.utility.tools.AppFunction;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.tools.Arguments;
import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Emit;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.*;

/**
 *
 * @author sjh
 */
public class BuildEntitiesIndex extends AppFunction {

    public static final Logger logger = Logger.getLogger("BatchSearch");

    public static void main(String[] args) throws Exception
    {
        (new BatchSearch()).run(Arguments.parse(args), System.out);
    }

    @Override
    public String getName() {
        return "build-entities-index";
    }

    @Override
    public String getHelpString() {
        return "galago build-entities-index <entitylist=path to entitylist> <entityindexfile= path to entity index file> <documentsindexedfile= path to document index>\n\n";
    }

    @Override
    public void run(Parameters parameters, PrintStream out) throws Exception {
        List<ScoredDocument> results;

        if (!(parameters.containsKey("query") || parameters.containsKey("queries")))
        {
            out.println(this.getHelpString());
            return;
        }

        if(!parameters.containsKey("entitylist") || !parameters.containsKey("entityindexfile") || !parameters.containsKey("documentsindexedfile"))
        {
            out.println(this.getHelpString());
            return;
        }

        // ensure we can print to a file instead of the commandline
        if (parameters.isString("outputFile"))
        {
            boolean append = parameters.get("appendFile", false);
            out = new PrintStream(new BufferedOutputStream(
                    new FileOutputStream(parameters.getString("outputFile"), append)), true, "UTF-8");
        }

        // get queries
        List<Parameters> queries;
        String queryFormat = parameters.get("queryFormat", "json").toLowerCase();
        switch (queryFormat)
        {
            case "json":
                queries = JSONQueryFormat.collectQueries(parameters);
                break;
            case "tsv":
                queries = JSONQueryFormat.collectTSVQueries(parameters);
                break;
            default: throw new IllegalArgumentException("Unknown queryFormat: "+queryFormat+" try one of JSON, TSV");
        }

        BufferedReader br1 = new BufferedReader(new FileReader(parameters.getString("entitylist")));
        String line;
        HashMap<String, Integer> entities = new HashMap<String, Integer>();
        while((line=br1.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken());
                //entities.add(to)
            }
            if(tokens.size()!=2) continue;
            entities.put(tokens.get(1).replace("_"," ").replace("-"," "),Integer.parseInt(tokens.get(0)));
        }

        Trie.TrieBuilder trie_builder  = Trie.builder().ignoreCase().onlyWholeWordsWhiteSpaceSeparated();
        for(String keyword : entities.keySet())
        {
            trie_builder = trie_builder.addKeyword(keyword);
        }
        Trie trie = trie_builder.build();
        BufferedWriter bw = new BufferedWriter(new FileWriter(parameters.getString("entityindexfile")));

        // open index
        Retrieval retrieval = RetrievalFactory.create(parameters);
        System.out.println("here");

        // record results requested
        int requested = (int) parameters.get("requested", 1000);

        // for each query, run it, get the results, print in TREC format
        HashSet<Integer> h = new HashSet<Integer>();
        Document.DocumentComponents p1 = new Document.DocumentComponents();
        org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(parameters);

        HashSet<Long> documents = new HashSet<Long>();

        for (Parameters query : queries)
        {
            String queryText = query.getString("text");
            System.out.println("text of the query is: "+queryText);
            String queryNumber = query.getString("number");


            query.setBackoff(parameters);
            query.set("requested", requested);

            // option to fold query cases -- note that some parameters may require upper case
            if (query.get("casefold", false))
            {
                queryText = queryText.toLowerCase();
            }

            if (parameters.get("verbose", false))
            {
                logger.info("RUNNING: " + queryNumber + " : " + queryText);
            }

            // parse and transform query into runnable form
            Node root = StructuredQuery.parse(queryText);

            // --operatorWrap=sdm will now #sdm(...text... here)
            if(parameters.isString("operatorWrap"))
            {
                if(root.getOperator().equals("root"))
                {
                    root.setOperator(parameters.getString("operatorWrap"));
                } else
                    {
                    Node oldRoot = root;
                    root = new Node(parameters.getString("operatorWrap"));
                    root.add(oldRoot);
                }
            }
            //String file_name=parameters.getString("entitylist");
            Node transformed = retrieval.transformQuery(root, query);
            String query_text = transformed.getText().replaceAll("\\s{2,}", " ").trim();



            if (parameters.get("verbose", false))
            {
                logger.info("Transformed Query:\n" + transformed.toPrettyString());
            }

            // run query
            results = retrieval.executeQuery(transformed, parameters).scoredDocuments;
            System.out.println("\n"+results.size()+"\n");
            for(ScoredDocument n:results)
            {
               // System.out.println(n.document);
               // System.out.println(n.score);
                if(!documents.contains(n.document)) {
                    documents.add(n.document);
                    String docname = retrieval.getDocumentName(n.document);
                    org.lemurproject.galago.core.parse.Document d = s.getDocument(docname, p1);
                    //System.out.println(d.text);
                    // String dtext1 = d.text.replace("<", " <").replace(">", "> ").toLowerCase();
                    String dtext = (d.contentForXMLTag("title") + " " + d.contentForXMLTag("paperAbstract")).toLowerCase().replace("<strong>", "").replace("</strong>", "").replaceAll("[(),.!?;:]", " $0 ").replaceAll("[^A-Za-z0-9. ]+", " $0 ");
                    Collection<Emit> named_entity_occurences = trie.parseText(dtext);
                    Set<String> named_entities = new HashSet<String>(); // Here set is created additionally to handle cases where named entity is present more than once in a document
                   /* if(named_entity_occurences.size()==0)
                    {
                        System.out.println(document+" "+dtext);
                    }*/
                    for (Emit named_entity_occurence : named_entity_occurences) {
                        bw.write("\n" + entities.get(named_entity_occurence.getKeyword()) + "\t" + n.document);
                    }
                }
            }
           /* org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
            org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(parameters);
            for(ScoredDocument n:results)
            {
                System.out.println(n.document);
                System.out.println(n.score);
            }
*/

        }

        if (parameters.isString("outputFile")) {
            out.close();
        }
    }

}
