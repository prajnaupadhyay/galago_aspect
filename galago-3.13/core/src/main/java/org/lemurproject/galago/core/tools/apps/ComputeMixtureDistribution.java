package org.lemurproject.galago.core.tools.apps;

import org.lemurproject.galago.core.aspects.Preprocess;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.queries.JSONQueryFormat;
import org.lemurproject.galago.utility.tools.AppFunction;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;


/**
 * Computes probability distribution over words given different aspects
 * @author jfoley
 */
public class ComputeMixtureDistribution extends AppFunction {

    @Override
    public String getName() {
        return "compute-mixture-distribution";
    }

    @Override
    public String getHelpString() {
        return "compute-mixture-distribution word_prob_file=<file containing the probability distribution estimated for the aspect> entity_prob_file=<file containing the pairwise probability of entities likely to be connected by the relationship described by the aspect estimated using PRA algorithm from TeKnowbase.> entity_prob_file1=<file containing the pairwise probability of entities likely to be connected by the relationship described by the aspect estimated using MetaPath2Vec algorithm from TeKnowbase.> termstatistics=<background probability distribution file> miiiixturefile=<file to store the mixture pobability distribution> lamda1=<parameter to mix query dependent and query independent component> lambda2=<parameter to mix the two inferencing components> --query=<query>";
    }

    @Override
    public void run(Parameters parameters, PrintStream out) throws Exception {

       // Retrieval index = RetrievalFactory.create(p);
        Preprocess pr = new Preprocess();
        String word_prob_file="";
        String entity_prob_file="";
        String entity_prob_file1="";

        String mixturefile="";
        String termstatistics="";

        List<Parameters> queries;
        String queryFormat = parameters.get("queryFormat", "json").toLowerCase();
        switch (queryFormat)
        {
            case "json":
                queries = JSONQueryFormat.collectQueries(parameters);
                break;
            case "tsv":
                queries = JSONQueryFormat.collectTSVQueries(parameters);
                break;
            default: throw new IllegalArgumentException("Unknown queryFormat: "+queryFormat+" try one of JSON, TSV");
        }


        if(parameters.containsKey("word_prob_file"))
        {
            word_prob_file = parameters.getString("word_prob_file");
        }
        else
        {
            out.println("word_prob_file is missing");
            System.exit(0);
        }

        if(parameters.containsKey("entity_prob_file"))
        {
            entity_prob_file = parameters.getString("entity_prob_file");
        }
        else
        {
            out.println("entity_prob_file is missing");
            System.exit(0);
        }

        if(parameters.containsKey("entity_prob_file1"))
        {
            entity_prob_file1 = parameters.getString("entity_prob_file1");
        }
        else
        {
            out.println("entity_prob_file1 file is missing");
            System.exit(0);
        }
        if(parameters.containsKey("mixturefile"))
        {
            mixturefile = parameters.getString("mixturefile");
        }
        else
        {
            out.println("mixturefile is missing");
            System.exit(0);
        }
        if(parameters.containsKey("termstatistics"))
        {
            termstatistics = parameters.getString("termstatistics");
        }
        else
        {
            out.println("termstatistics file is missing");
        }
        double lambda_1=0.5;
        double lambda_2=0.5;

        if(parameters.containsKey("lambda1"))
        {
            lambda_1 = parameters.getDouble("lambda1");
        }

        if(parameters.containsKey("lambda2"))
        {
            lambda_2 = parameters.getDouble("lambda2");
        }

        HashMap<Integer, Double> term_statistics = new HashMap<Integer, Double>();
        HashMap<String, Integer> term_statistics_mapped = new HashMap<String, Integer>();

        BufferedReader br3 = new BufferedReader(new FileReader(termstatistics));
        String line;
        double total = 0.0;
        while((line=br3.readLine())!=null)
        {
            StringTokenizer tok = new StringTokenizer(line,"\t");
            ArrayList<String> tokens = new ArrayList<String>();
            while(tok.hasMoreTokens())
            {
                tokens.add(tok.nextToken().replace("_"," "));
                //entities.add(to)
            }
            if(tokens.size()!=4) continue;

            term_statistics.put(Integer.parseInt(tokens.get(3)),Double.parseDouble(tokens.get(1)));
            term_statistics_mapped.put(tokens.get(0),Integer.parseInt(tokens.get(3)));
            total = total + Double.parseDouble(tokens.get(1));
        }

        for(int s:term_statistics.keySet())
        {
            term_statistics.put(s,term_statistics.get(s)/total);
        }

        for (Parameters query : queries)
        {
            String queryText = query.getString("text");
            System.out.println("text of the query is: " + queryText);
            pr.computeMixtureDistribution(word_prob_file, entity_prob_file, entity_prob_file1, lambda_1, lambda_2, true, queryText, mixturefile, termstatistics, term_statistics, term_statistics_mapped);
        }
    }
}
