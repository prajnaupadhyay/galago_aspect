package org.lemurproject.galago.core.tools.apps;

import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.retrieval.ScoredDocument;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.lemurproject.galago.utility.queries.JSONQueryFormat;
import org.lemurproject.galago.utility.tools.AppFunction;
import org.lemurproject.galago.utility.Parameters;
import org.lemurproject.galago.utility.tools.Arguments;
import org.ahocorasick.trie.Trie;
import org.ahocorasick.trie.Emit;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.*;

public class GetGoldStandard extends AppFunction
{
    public static final Logger logger = Logger.getLogger("BatchSearch");

    public static void main(String[] args) throws Exception
    {
        (new org.lemurproject.galago.core.tools.apps.GetGoldStandard()).run(Arguments.parse(args), System.out);
    }

    @Override
    public String getName()
    {
        return "get-gold-standard";
    }

    @Override
    public String getHelpString()
    {
        return this.getName()+" --index=<path to index> --verbose --corpus=<path to indexed corpus> --processingModel=getalldocs --requested=<#documents you want as result> --query=<query name>  --aspectname=<aspect name> --outfile=<file to store the results>";
    }

    @Override
    public void run(Parameters parameters, PrintStream out) throws Exception {
        List<org.lemurproject.galago.core.retrieval.ScoredDocument> results;

        if (!(parameters.containsKey("query") || parameters.containsKey("queries")))
        {
            out.println(this.getHelpString());
            return;
        }
        if(parameters.containsKey("entitylist"))
        {
            System.out.println("yeah");
        }
        String aspect="";
        if(parameters.containsKey("aspectname"))
        {
            aspect = parameters.getString("aspectname");
        }
       /* else
        {
            out.println(this.getHelpString());
            return;
        }*/
        String outfile="";

        if(parameters.containsKey("outfile"))
        {
            outfile = parameters.getString("outfile");
        }
        else
        {
            out.println(this.getHelpString());
            return;
        }

        // ensure we can print to a file instead of the commandline
        if (parameters.isString("outputFile"))
        {
            boolean append = parameters.get("appendFile", false);
            out = new PrintStream(new BufferedOutputStream(
                    new FileOutputStream(parameters.getString("outputFile"), append)), true, "UTF-8");
        }

        // get queries
        List<Parameters> queries;
        String queryFormat = parameters.get("queryFormat", "json").toLowerCase();
        switch (queryFormat)
        {
            case "json":
                queries = JSONQueryFormat.collectQueries(parameters);
                break;
            case "tsv":
                queries = JSONQueryFormat.collectTSVQueries(parameters);
                break;
            default: throw new IllegalArgumentException("Unknown queryFormat: "+queryFormat+" try one of JSON, TSV");
        }

        // open index
        org.lemurproject.galago.core.retrieval.Retrieval retrieval = org.lemurproject.galago.core.retrieval.RetrievalFactory.create(parameters);
        System.out.println("here");
        String mode="";
        // record results requested
        int requested = (int) parameters.get("requested", 1000);
        BufferedWriter bw1 = new BufferedWriter(new FileWriter(outfile,true));

        // for each query, run it, get the results, print in TREC format
        HashSet<Integer> h = new HashSet<Integer>();
        for (Parameters query : queries)
        {
            String queryText = query.getString("text");
            System.out.println("text of the query is: "+queryText);
            String queryNumber = query.getString("number");


            query.setBackoff(parameters);
            query.set("requested", requested);

            // option to fold query cases -- note that some parameters may require upper case
            if (query.get("casefold", false))
            {
                queryText = queryText.toLowerCase();
            }

            if (parameters.get("verbose", false))
            {
                logger.info("RUNNING: " + queryNumber + " : " + queryText);
            }

            // parse and transform query into runnable form
            org.lemurproject.galago.core.retrieval.query.Node root = org.lemurproject.galago.core.retrieval.query.StructuredQuery.parse(queryText+" "+aspect);

            // --operatorWrap=sdm will now #sdm(...text... here)
            if(parameters.isString("operatorWrap"))
            {
                if(root.getOperator().equals("root"))
                {
                    root.setOperator(parameters.getString("operatorWrap"));
                } else
                    {
                    org.lemurproject.galago.core.retrieval.query.Node oldRoot = root;
                    root = new org.lemurproject.galago.core.retrieval.query.Node(parameters.getString("operatorWrap"));
                    root.add(oldRoot);
                }
            }
            Node transformed = retrieval.transformQuery(root, query);

            if (parameters.get("verbose", false))
            {
                logger.info("Transformed Query:\n" + transformed.toPrettyString());
            }

            // run query
            results = retrieval.executeQuery(transformed, parameters).scoredDocuments;
            System.out.println("\n"+results.size()+"\n");
            org.lemurproject.galago.core.parse.Document.DocumentComponents p1 = new org.lemurproject.galago.core.parse.Document.DocumentComponents();
            org.lemurproject.galago.core.tools.Search s = new org.lemurproject.galago.core.tools.Search(parameters);
            int count=0;
            for(org.lemurproject.galago.core.retrieval.ScoredDocument n:results)
            {
                //System.out.println(n.document);
               // System.out.println(n.score);
                String docname = retrieval.getDocumentName(n.document);
                org.lemurproject.galago.core.parse.Document d = s.getDocument(docname, p1);
                String title = d.contentForXMLTag("title").toLowerCase().replace("\n"," ");
                if(title.contains(queryText) && title.contains(aspect))
               // if(title.contains(queryText))
                {
                    count++;
                    bw1.write(n.document+"\t"+title+"\t"+d.contentForXMLTag("paperAbstract").toLowerCase().replace("\n"," ")+"\n");
                    if(count==1000) break;
                }
            }
        }
        bw1.close();

        if (parameters.isString("outputFile")) {
            out.close();
        }
    }
}
